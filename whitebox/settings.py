from . import pages, db, web, utils, datahands, email
import tornado.web, time

class APIHandler(web.BaseHandler):
    @tornado.web.authenticated
    async def get(self, text=None):
        if not self.isapi():
            self.set_status(400)
            self.make_page("Denied", "", msgtext="Denied by account limits.")
            return

        torevoke = self.get_argument("revoke")
        async with db.db_engine.begin() as conn:
            if torevoke and (await conn.execute(db.apikey.delete().where(
                db.apikey.c.user == self.current_user["uuid"],
                db.apikey.c.name == torevoke
            ))).rowcount > 0:
                await self.update_user({"apilen": -2}, conn)
            apikeys = await db.get_row(db.apikey, "user", self.current_user["uuid"], conn, offpair=self.pagepair())

        akrows = [[key["name"], key["created_at"], key["last_used"], key["use_count"], key["name"]] for key in apikeys]
        table = pages.build_table_frag('api_keys', akrows)

        token = self.get_csrf("/settings/api")
        self.make_page(f'API keys for {self.current_user["username"]}',
            pages.get_fragment('api-settings', keys={
                "token": token,
                "apikeys": table,
                'page-links:pagelinks': datahands.gen_pagelinks(akrows, self.page, self.page_size, self.current_user["apilen"]),
            }), msgtext=(text or None)
        )

    @tornado.web.authenticated
    async def post(self):
        if not self.isapi():
            self.set_status(400)
            self.make_page("Denied", "", msgtext="Denied by account limits.")
            return

        if not self.verify_csrf():
            return self.get('CSRF token validation failed.')
        name = self.get_argument("name")
        if not name or not utils.check_str(name):
            return self.get('Invalid API name')
        async with db.db_engine.begin() as conn:
            if await db.get_row(db.apikey, "user", self.current_user["uuid"], conn, name=name):
                return self.get('An API key is already named that')

        keydat = None
        async with db.db_engine.begin() as conn:
            while True:
                keydat = utils.random_str(64)
                if not await db.get_row(db.apikey, "uuid", keydat, conn, True):
                    break
        data = {
            "uuid": keydat,
            "name": name,
            "user": self.current_user["uuid"],
            "created_at": int(time.time()),
            "scope": None,
        }
        async with db.db_engine.begin() as conn:
            await self.update_user({"apilen": -1}, conn)
            await conn.execute(db.apikey.insert(), data)

        return await self.get("The API key will vanish upon a page refresh; so note this down now: " + keydat)

class SettingsHandler(web.BaseHandler):
    @tornado.web.authenticated
    def get(self, text=None):
        token = self.get_csrf("/settings")
        self.make_page(f'Settings for {self.current_user["username"]}',
            pages.get_fragment('settings-page', keys={
                "token": token,
                "currname": self.current_user["username"],
                "curremail": self.current_user["email"],
                "currdesc": self.current_user["description"],
                "ispubfavs": "checked" if self.current_user["public_favourites"] else "",
            }, indent=0), msgtext=(text or None)
        )

    @tornado.web.authenticated
    async def post(self):
        if not self.verify_csrf():
            return self.get('CSRF token validation failed.')
        userdata = await db.get_user(self.current_user["username"], dopass=True)

        ndata = {}
        for (field, forment) in (
            ("username", "username"),
            ("description", "description"),
            ("public_favourites", "pubfavs"),
            ("email", "email"),
        ):
            ndata[field] = self.get_argument(forment)
        ndata["public_favourites"] = bool(ndata["public_favourites"])

        if not utils.check_str(ndata["username"]):
            return self.get('Username invalid.')
        if not email.check_email(ndata["email"]):
            return self.get('Email invalid.')

        curpass = self.get_argument('currentpass', 'hope_nobody_puts_this_as_their_pass')
        if not utils.check_pass(curpass):
            return self.get('Invalid password')
        salt = utils.hexcode(userdata["password"].split("$")[1], decode=True)
        if userdata["password"] != await utils.passcode(curpass, salt=salt):
            return self.get('Invalid password')

        password, confirmer = (
            self.get_argument('password', 'the_user_is_dumb'),
            self.get_argument('confirmer', 'for_not_repeating_the_password'),
        )
        if password:
            if password != confirmer:
                return self.get('Passwords given are not the same.')
            if not utils.check_pass(password):
                return self.get("Password doesn't fit the requirements (min 8 chars/lower/upper/number/symbol).")
            ndata["password"] = await utils.passcode(password)

        async with db.db_engine.begin() as conn:
            await self.update_user(ndata, conn)

        self.redirect("/user/" + self.current_user["username"])
