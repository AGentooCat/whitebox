from . import db, utils, pages, web, config, email, user as muser
import time

class LogoutHandler(web.BaseHandler):
    def get(self, text=None):
        self._logout()
        self.redirect(self.get_argument('next', '/'))

class LoginHandler(web.BaseHandler):
    def _check_login(self):
        if self.current_user:
            self.redirect(self.get_argument('redirect', '/'))
            return True
    def get(self, text=None):
        if self._check_login():
            return

        token = self.get_csrf("/login")
        c = self.get_argument('next', '/')
        self.make_page('Login', pages.get_fragment('login-form', indent=0, token=token, redir=c), msgtext=(text or ''), path=c)

    async def post(self):
        if self._check_login():
            return

        if not self.verify_csrf():
            return self.get('CSRF token validation failed.')

        username, password = (
            self.get_argument('username'),
            self.get_argument('password', 'hope_nobody_puts_this_as_their_pass'),
        )
        user = await db.get_user(username, dopass=True)
        if not user:
            return self.get('This user is not registered.')
        if not user["activated"]:
            return self.get('Your account is not activated; please contact an admin.')
        if user["type"] == muser.UserSType["banned"]:
            whobanned = await db.get_uuid(db.account, user["banuuid"], None, True)
            return self.get([
                'This user has been banned by ' + whobanned["username"] + '.',
                'Reason: ' + user["banreason"]
            ])

        if not config.confdat.web.bypass_logins:
            salt = utils.hexcode(user['password'].split('$')[1], decode=True)

            hush = await utils.passcode(password, salt=salt)
            if hush != user['password']:
                return self.get('Wrong password.')

        self._login(user)

class RegisterKicker(web.BaseHandler):
    def get(self, text=None):
        if self.current_user:
            self.redirect(self.get_argument('redirect', '/'))
            return

        self.set_status(403)
        c = self.get_argument('next', '/')
        self.make_page('Registration disabled', '<div class="fail-message">Registrations are disabled. Sorry.</div>', path=c)

class RegisterHandler(LoginHandler):
    def get(self, text=None):
        if self._check_login():
            return

        token = self.get_csrf("/register")
        c = self.get_argument('next', '/')
        self.make_page('Register', pages.get_fragment('register-form', indent=0, token=token, redir=c), msgtext=(text or ''), path=c)

    async def post(self):
        if self._check_login():
            return

        username, emailaddr, password, confirmer = (
            self.get_argument('username'),
            self.get_argument("email"),
            self.get_argument('password', 'the_user_is_dumb'),
            self.get_argument('confirmer', 'for_not_repeating_the_password'),
        )
        ftext = None
        if not self.verify_csrf():
            ftext = 'CSRF token validation failed.'
        elif not self.get_argument("tosread"):
            ftext = 'Please read the terms of service.'
        elif password != confirmer:
            ftext = 'Passwords given are not the same.'
        elif not utils.check_pass(password):
            ftext = "Password doesn't fit the requirements below."
        elif not utils.check_str(username):
            ftext = 'Username invalid.'
        elif not email.check_email(emailaddr):
            ftext = "Email invalid."
        elif await db.get_user(username):
            ftext = 'This user is already registered.'
        elif await db.get_row(db.account, "email", emailaddr, None, True):
            ftext = 'This email address is already used.'
        if ftext:
            return self.get(ftext)

        hush = await utils.passcode(password)
        user = {
            'uuid': utils.get_uuid(),
            'username': username,
            'password': hush,
            'date': time.time(),
            "activated": not config.confdat.web.require_activation,
            "email": emailaddr,
        }
        async with db.db_engine.begin() as conn:
            await conn.execute(db.account.insert().values(user))
            user = await db.get_user(user["uuid"], uuid=True, conn=conn)

        self._login(user, register=True)
        if not user["activated"]:
            return super().get("Your account needs activation; please contact an admin.")
