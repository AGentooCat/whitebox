from . import utils, config, ipc, db, plugin, thread
import asyncio, os, logging
import tornado.web
log = logging.getLogger(__name__)

logins = {}

class BaseHandler(tornado.web.RequestHandler):
    def get_argument(self, arg, default='', allowed=None):
        args = self.request.arguments.get(arg)
        if not (args and args[0]):
            return default
        arg = args[0].decode()
        if allowed and arg not in allowed:
            return default
        return arg

    def _login(self, user, register=False):
        user["password"] = "[blanked out]"
        token = ipc.request_ipc("register" if register else "login_user", data=user)
        if token:
            self.set_cookie('_token', token, expires_days=None)
            self.redirect(self.get_argument('next', '/'))

    def _logout(self):
        if token := self.get_cookie("_token"):
            ipc.request_ipc("logout", token=token)
            self.clear_cookie("_token")

    def get_current_user(self):
        if not (cookie := self.get_cookie("_token")):
            return None
        self.token = token = cookie
        is_logged = ipc.request_ipc("check_login", token=token)
        if not is_logged:
            if cookie:
                self._logout()
            return self._close_folder()
        login_data = is_logged[0]

        if login_data["type"] == user.UserSType["banned"] or not login_data["activated"]:
            # user was banned/deactivated while being logged in
            if cookie:
                self._logout()
            return self._close_folder()
        return login_data

    async def update_user(self, data, conn):
        if "password" in data:
            data.pop("password")
        if not (data := ipc.request_ipc("update_user", token=self.token, data=data)):
            return
        await conn.execute(db.account.update().where(
            db.account.c.uuid == self.current_user['uuid']
        ), data)
        self.current_user |= data

    def isadmin(self):
        return self.current_user and self.current_user["type"] == user.UserSType["admin"]
    def isapi(self):
        return self.current_user and (self.isadmin() or self.current_user["api_allowed"])

    def prepare(self):
        self.page = int(self.get_argument('page', 1))
        self.page_size = min(int(self.get_argument('size', config.confdat.web.page_size)), config.confdat.web.page_size)

    def pagepair(self, plusone=False):
        return ((self.page - 1) * self.page_size, self.page_size + int(plusone))

    def send404(self, text=None):
        self.set_status(404)
        text = f'<div class="fail-message">{text}</div>' \
                if text else pages.statics[r"/errors/404.html"][1]
        self.make_page('404 Not Found', text)

    def get_csrf(self, next_url):
        return ipc.request_ipc("get_csrf", next_url=next_url)

    def verify_csrf(self):
        token = self.get_argument("CSRFToken")
        return token and ipc.request_ipc("verify_csrf", token=token, url=self.request.path)

    def _get_folder(self, usethis=None, redirect=True, reset=True, floff=None):
        fuse, uuse = usethis or (None, None)
        if not (token := fuse or self.get_cookie("_upload_token")):
            return None
        folder = ipc.request_ipc("get_folder", token=token, user_token=(uuse or self.token), reset_time=reset, reset_pause=reset, floff=floff)
        if not folder:
            if not fuse:
                self.clear_cookie("_upload_token")
            if redirect:
                self.redirect("/upload")
            return None

        self._ftoken = token
        self._folder = folder
        return folder

    def _close_folder(self):
        if not self._get_folder(redirect=False):
            return
        ipc.request_ipc("close", token=self._ftoken, user_token=self.token, foldat=None)
        self.clear_cookie("_upload_token")

    def _check_limit(self, length=None):
        return (self.limit > 0 and (length or self.bytes_read) > self.limit)

    def make_page(self, title, body, msgtext='', path=None):
        path = utils.url_escape(path or self.request.path)
        if self.current_user:
            folder = self._get_folder(False, False)
            prefix = "Upload"
            if folder["infinal"]:
                prefix = "Finaliz"
            elif folder["existing"]:
                prefix = "Append"
            inprog = (prefix + "ing: " + folder["name"]) if folder else 'Upload'
            nfrag = pages.get_fragment('navbar-side-logged', keys={
                "uname": user.color_user(self.current_user, withlink=True),
                "inprogress": inprog
            }, nodiv=True, next=path)
        else:
            nfrag = pages.get_fragment('navbar-side-notlogged', nodiv=True, next=path)

        if not isinstance(msgtext, list):
            msgtext = [msgtext]
        msgtext = '<br>'.join(utils.xhtml_escape(i) for i in filter(lambda i: i, msgtext))
        if config.confdat.web.bypass_logins:
            msgtext = "This instance is currently bypassing logins = logins are passwordless. <b>Don't use this instance.</b>" + ('<br><br>' + msgtext if msgtext else "")
        if msgtext:
            msgtext = f'<div class="fail-message">{msgtext}</div>'

        self.set_header('Content-Type', 'text/html')
        self.write(pages.get_fragment("skeleton", keys={
            "title": title,
            "cssver": pages.statics['/main.css'][2],
            "nfrag": nfrag,
            "msgtext": msgtext or '',
            "body": body
        }, indent=0, nodiv=True))

    async def send_stream(self, stream):
        self.flush()
        output = self.detach()
        socket = output.socket.dup()
        output.close()
        socket.setblocking(True)

        def dosend(stream, sock):
            for chunk in stream:
                while chunk:
                    try:
                        sent = sock.send(chunk)
                    except BlockingIOError:
                        continue
                    except Exception:
                        return
                    if sent == len(chunk):
                        break
                    chunk = chunk[sent:]
        await thread.run_in_thread(dosend, stream, socket)
        try:
            socket.close()
        except Exception:
            pass

from . import pages, upload, file, login, \
        search, user, settings, api, status # noqa: E402
# *Handlers subclassing (this=web.)BaseHandler try to import this file
# causing an import loop (partial module "web" doesn't have "BaseHandler")

class Root(BaseHandler):
    async def get(self):
        flrows = []
        tfrag = ''
        folders = ipc.request_ipc("fetch_cache", rtype="folder", off=0, len=config.confdat.web.page_size)[0]

        for (n, i) in enumerate(folders):
            flrows.append([i["thumb"], i['name'], i['date'], i['category'], i['creator'], (i["views"], i["favourites"])])
        tfrag = pages.build_table_frag('folder_results', flrows, klasse="list")

        self.make_page("Main page", pages.get_fragment("root", recentuploads=tfrag))

class FourOhFour(BaseHandler):
    def get(self):
        path = self.request.path
        try:
            data = pages.get_static(path, True)
        except FileNotFoundError:
            return self.send404()
        self.write(data[0])

class TermsOfService(BaseHandler):
    def get(self):
        self.make_page("Terms of Service", pages.get_fragment("terms-of-service-page"))

async def webloop(conf, sock):
    if noupload := config.confdat.web.disable_upload:
        noupload = upload.UploadKicker, api.APIUploadKicker
    else:
        noupload = None, None
    noregister = config.confdat.web.disable_registration
    routes = [
        (r"/", Root),

        (r"/upload", noupload[0] or upload.UploadHandler),
        (r"/upload/new", noupload[0] or upload.UploadFormHandler),
        (r"/upload/status", noupload[0] or upload.UploadFolderHandler),
        (r"/upload/thumb.(png|jpg|jpeg)", noupload[0] or upload.UploadThumbHandler),
        (r"/upload/upload", noupload[0] or upload.IncomingFile),

        (r"/folder/([a-zA-Z0-9_.-]*)/?$", file.FolderHandler),
        (r"/fthumb/([a-zA-Z0-9_.-]*).(png|jpg|jpeg)", file.FolderThumbHandler),
        (r"/folder/([a-zA-Z0-9_.-]*)/(.*)/$", file.FolderHandler),
        (r"/folder/([a-zA-Z0-9_.-]*)/(.*)", file.FileHandler),
        (r"/download/([a-zA-Z0-9_.-]*)\.([a-z0-9_]*)$", file.DownloadHandler),

        (r"/append/([a-zA-Z0-9_.-]*)", noupload[0] or upload.AppendUploadHandler),

        (r"/login", login.LoginHandler),
        (r"/register", login.RegisterKicker if noregister else login.RegisterHandler),
        (r"/logout", login.LogoutHandler),
        (r"/tos", TermsOfService),

        (r"/search", search.SearchHandler),

        (r"/user/([a-zA-Z0-9_.-]*)", user.UserHandler),
        (r"/user/([a-zA-Z0-9_.-]*)/ban", user.BanHandler),
        (r"/user/([a-zA-Z0-9_.-]*)/favourites", user.FavouriteHandler),

        (r"/settings", settings.SettingsHandler),
        (r"/settings/api", settings.APIHandler),

        (r"/api/upload/append/([a-zA-Z0-9_.-]*)", noupload[1] or api.APIAppendUploadHandler),
        (r"/api/upload/new/([a-zA-Z0-9_.-]*)", noupload[1] or api.APINewUploadHandler),
        (r"/api/upload/file/(.*)", noupload[1] or api.APIFileUploadHandler),
        (r"/api/upload/url", noupload[1] or api.APINewURLHandler),
        (r"/api/upload/commit", noupload[1] or api.APIFinishFolderHandler),
        (r"/api/upload/close", noupload[1] or api.APICloseFolderHandler),

        (r"/api/user", api.APIUserHandler),
        (r"/api/folder", api.APIFolderHandler),
        (r"/api/folder/delete/([a-zA-Z0-9_.-]*)", api.APIFolderDeleteHandler),

        (r"/status", status.StatusHandler),
        *plugin.run_in_category("WebRoutes", [], nextonnone=True)
    ]
    pages.init_pages(routes)

    app = tornado.web.Application(routes,
        cookie_secret=os.urandom(256),
        login_url='/login',
        default_handler_class=FourOhFour)
    serv = tornado.httpserver.HTTPServer(app,
               chunk_size=config.confdat.web.chunk_size,
               max_body_size=config.confdat.data.max_upload_size + 10485760)
    serv.add_sockets(sock)
    if not config.confdat.web.enable_request_logs:
        tornado.log.access_log.info = tornado.log.access_log.debug
    log.info('Now listening on %s:%d', conf.host, conf.port)

    await asyncio.Event().wait()
