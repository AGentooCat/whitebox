from . import ipc, web, user, pages, utils
import tornado.web

class StatusHandler(web.BaseHandler):
    @tornado.web.authenticated
    def get(self):
        if user.UserType[self.current_user["type"]][0] != "admin":
            return self.send404()

        folders = ipc.request_ipc("fetch_cache", rtype="folder", off=0, len=None)[0]
        users = ipc.request_ipc("fetch_cache", rtype="user", off=0, len=None)[0]
        meta = ipc.request_ipc("fetch_cache", rtype="meta", off=0, len=None)

        flrows = [[i["thumb"], i['name'], i['date'], i['category'], i["creator"], (i["views"], i["favourites"])] for i in folders]
        acrows = [[i, str(i["foldlen"]), i['date']] for i in users]

        flen, alen = len(flrows), len(acrows)
        flrows = pages.build_table_frag("folder_results", flrows, klasse="list")
        acrows = pages.build_table_frag("user_entries", acrows, klasse="list")
        self.make_page("Admin status page", pages.get_fragment("status-page", keys={
            "folnum": meta["folders"],
            "accnum": meta["users"],
            "filnum": meta["files"],
            "fdsize": "{} ({} bytes)".format(utils.get_human_size(meta["total_size"]), meta["total_size"]),
            "lastfold": flen,
            "lastuser": alen,

            "lastfolders": flrows,
            "lastusers": acrows,
        }))
