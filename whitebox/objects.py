from . import errors, utils
import os

def getobjname(hush, create=False):
    if isinstance(hush, bytes):
        hush = utils.hexcode(hush)

    objdir = utils.getpath('objects/')
    for i in [0, 2, 4, 6]:
        objdir += hush[i:i+2] + '/'
    if create:
        utils.mkdir(objdir)

    return objdir + '/' + hush

async def putobject(conn, data, now, knowns):
    bash = data._hash
    filename = getobjname(bash, create=True)
    found, same = utils.filecontains(filename, data)

    if not found:
        os.rename(data._backing_file, filename)
        data.close()
    elif not same:
        dest = utils.getpath("broken_hash")
        utils.mkdir(dest)
        dest = dest + "/" + utils.random_str()
        os.rename(data._backing_file, dest)
        data.close()
        raise errors.HashCollisionError(data._hash.hex(), filename, dest)

    block = {
        'uuid': data.uuid,
        'date': now,
        'hash': bash,
        'type': data.mime,
        'name': data.name,
        'size': data.size,
    }

    nuid = None
    for fent in knowns:
        if fent["hash"] == block["hash"]:
            nuid = fent["uuid"]
            break
    if not nuid and same:
        from . import db # here otherwise circular import of something (i dunno)
        if ret := await db.get_row(db.file, 'hash', bash, conn, True):
            nuid = ret["uuid"]
        else:
            # BAD DB STATE: file is in its place but the db doesn't have it
            same = False
    if nuid:
        block["uuid"] = nuid

    return (bash, same, block)

# python's file streams implement iter() where next() calls readline
# which alloc-reads everything until a line-seperator.
# you can see this being a memory-disaster for specific files.
def FILEnext(self):
    buf = utils.fillbuf(8192, self.read)
    if not buf:
        raise StopIteration
    return buf

def getobject(bash, stream=False):
    filename = getobjname(bash)
    f = open(filename, 'rb')
    f.__next__ = FILEnext
    if stream:
        return f
    d = f.read()
    f.close()
    return d

def statobject(bash, size):
    filename = getobjname(bash)

    if not os.access(filename, os.R_OK):
        return False
    return os.stat(filename).st_size == size
