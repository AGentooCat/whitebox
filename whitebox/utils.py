import os, stat, hashlib, uuid, tempfile, magic, mmap, \
        string, pickle, struct, secrets
import tornado.escape, werkzeug.http
from . import config, errors, thread

class Empty:
    def __init__(self, **kwargs):
        for i in kwargs:
            setattr(self, i, kwargs[i])

lowercase = list(string.ascii_lowercase)
uppercase = list(string.ascii_uppercase)
numbers = list(string.digits)
symbol = ['_', '.', '-']
more_symbol = [
    ' ', '^', '+', ',', '(', ')', '[', ']', '?', '@', '*', '!'
]

alphabet = lowercase + uppercase + numbers + symbol

def check_str(ing):
    if not (ing and ing.isprintable()):
        return False
    return all(i in alphabet for i in ing)

def mkdir(path):
    done = ""
    for part in path.split('/'):
        done += "/" + part
        try:
            os.mkdir(done)
        except FileExistsError as e:
            st = os.stat(done)
            isdir = stat.S_ISDIR(st.st_mode)
            if not isdir:
                raise FileExistsError(e)

def random_str(length=64):
    return ''.join([alphabet[secrets.randbelow(len(alphabet))] for _ in range(length)])

def hexcode(data, decode=False):
    if decode:
        return bytes.fromhex(data)
    return data.hex()

def getpath(path):
    if not config.confdat:
        raise errors.NoBasePathError
    return os.path.join(config.confdat.data.maindir, path)

def filecontains(name, data):
    if not os.access(name, os.F_OK):
        return (False, False)
    fd = data._backing_file
    if not isinstance(fd, str):
        fd = fd.name
    ffd = open(fd, "rb")
    ln = ffd.seek(0, os.SEEK_END)
    if ln == 0:
        data = []
        ismap = False
    else:
        ismap = True
        data = mmap.mmap(ffd.fileno(), 0, mmap.MAP_SHARED, mmap.PROT_READ)
    offset = 0
    with open(name, 'rb') as fd:
        while buf := fd.read(16384):
            if buf != data[offset:offset+len(buf)]:
                if ismap:
                    data.close()
                return (True, False)
            offset += len(buf)
    retval = offset == len(data)
    if ismap:
        data.close()
    return (True, retval)

def get_temp():
    tpath = getpath("temp/")
    mkdir(tpath)
    tempfile.template = tpath

    temp = tempfile.mkstemp()
    os.close(temp[0])
    return open(temp[1], 'w+b')

def remove(file):
    try:
        os.remove(file)
    except IsADirectoryError:
        rmdir(file)
    except FileNotFoundError:
        pass

def rmdir(path):
    for file in os.listdir(path):
        file = path + "/" + file
        if stat.S_ISDIR(os.stat(file).st_mode):
            rmdir(file)
        remove(file)
    os.rmdir(path)

def clean_temp():
    path = getpath("temp/")
    for ent in os.listdir(path):
        remove(path + '/' + ent)

def get_mime(data):
    if isinstance(data, str):
        return magic.detect_from_filename(data).mime_type
    return magic.detect_from_content(data).mime_type

def norm(name):
    name = os.path.normpath(name)
    while name.startswith(("../", "./", "/")):
        name = name[1:]
    return os.path.normpath(name)

def _passcode(password, salt=None):
    salt = salt or os.urandom(64)
    # h[a-z]sh ==> ["hash", "hush", some others i dont know, can i haz engrish]
    if isinstance(password, str):
        password = password.encode()
    hush = hashlib.pbkdf2_hmac('blake2b', password, salt, 1000000)
    return '$' + hexcode(salt) + '$' + hexcode(hush)
async def passcode(password, salt=None, is_async=True):
    if is_async:
        return await thread.run_in_thread(_passcode, password, salt)
    return _passcode(password, salt)

def check_pass(password):
    if len(password) < 8:
        return False

    chklst = lowercase + uppercase + numbers + more_symbol + symbol
    return all(i in chklst for i in password)

def get_uuid():
    return str(uuid.uuid4()).upper()

KB = 1024.0
MB = KB * 1024.0
GB = MB * 1024.0
TB = GB * 1024.0
def get_human_size(size):
    if size < 0:
        return ''

    def s(size):
        return str(round(size, 2))

    if size < KB:
        return f'{size}b'
    size = float(size)

    for txt in ("KiB", "MiB", "GiB"):
        size /= 1024
        if size < 1000:
            return f'{s(size)} {txt}'

    return f'{s(size/1024)} TiB'

def get_range_list(req, maxsize):
    req = [i.strip() for i in req.split(',')]
    ranges = []
    wrange = Exception("Wrong range given.")
    for (n, i) in enumerate(req):
        start, _, end = i.partition("-")
        try:
            start = int(start)
            end = int(end) if end.strip() else -1
        except ValueError as e:
            raise wrange from e
        if not _:
            ranges.append((start, maxsize))
            if n + 1 < len(req):
                # who sends a "Range: bytes=100-200, 300, 400"??
                raise wrange
            continue
        if (end == -1) or (end + 1 > maxsize):
            end = maxsize
        elif (end < start) or (start + 1 > maxsize):
            raise wrange
        else:
            end += 1
        ranges.append((start, end))
    return ranges

def parse_header_line(line):
    _, opts = werkzeug.http.parse_options_header(line)
    key, _, val = line.partition(';')[0].rpartition(':')
    return key.strip(), val.strip(), opts

def get_multipart_heads(strm, boundary):
    if (init := strm.readline()) != boundary + b'\r\n':
        return
    heads = {}
    used = len(init)
    while (line := strm.readline()):
        used += len(line)
        if line == b'\r\n':
            break
        key, val, opts = parse_header_line(line.strip().decode())
        if opts:
            heads[key] = {val: None} | opts
        else:
            heads[key] = val
    return heads, used

xhtml_escape = tornado.escape.xhtml_escape
xhtml_descape = tornado.escape.xhtml_unescape
url_descape = tornado.escape.url_unescape
url_escape = lambda url: tornado.escape.url_escape(url, plus=False)

def fillbuf(size, func):
    data = b''
    while size > 0:
        if not (buf := func(size)):
            break
        data += buf
        size -= len(buf)
    return data

SIZE = struct.Struct(">L")
assert(SIZE.size == 4)

tosuper = None

def recvobj(sock):
    data = sock.recv(4)
    if not data:
        return None
    olen = SIZE.unpack(data)[0]
    data = pickle.loads(fillbuf(olen, sock.recv))
    if isinstance(data, Exception):
        raise data
    return data

def sendobj(sock, data):
    data = pickle.dumps(data)
    olen = SIZE.pack(len(data))
    sock.send(olen)
    return sock.send(data)
