from . import utils, objects, errors, cache, config
import sqlalchemy, sqlalchemy.ext.asyncio

def _doinitplz():
    raise errors.NoDatabaseError
db_engine = utils.Empty(begin=_doinitplz, connect=_doinitplz, dispose=False)
db_meta = None

class ColumnDef:
    def __init__(self, name, typ, **kwargs):
        self.name = name
        self.type = typ
        self.args = kwargs
type_map = {
    int: sqlalchemy.Integer,
    str: sqlalchemy.String,
    bytes: sqlalchemy.LargeBinary,
    bool: sqlalchemy.Boolean,
}
def type_get(ptype):
    if not isinstance(ptype, tuple):
        return type_map[ptype]
    return type_map[ptype[0]](ptype[1])

def gen_table(table_name, columns):
    global db_meta
    if not db_meta:
        db_meta = sqlalchemy.MetaData()
    table = sqlalchemy.Table(table_name, db_meta)
    for column in columns:
        if isinstance(column, tuple):
            column = ColumnDef(column[0], column[1])
        table.append_column(sqlalchemy.Column(column.name, type_get(column.type), **column.args))
    return table

file = gen_table('file', [
    ColumnDef('uuid', (str, 36), primary_key=True),
    ('date', int),
    ('hash', (bytes, 64)),
    ColumnDef("type", str, default='application/octet-stream'),
    ('name', str),
    ('size', int),
])
folder = gen_table('folder', [
    ColumnDef('uuid', (str, 36), primary_key=True),
    ColumnDef('name', str, primary_key=True),
    ('category', str),
    ('creator', (str, 36)),
    ('date', int),
    ('size', int),
    ColumnDef('views', int, default=0),
    ColumnDef('downloads', int, default=0),
    ColumnDef('favourites', int, default=0),
    ColumnDef('description', str, default=''),
    ('len', int),
    ('thumb', (str, 36)),
])
history = gen_table("history", [
    ("uuid", str),
    ("date", int),
    ("table", str),
    ("field", str),
    ("data", str),
])
folder_thumb = gen_table("folder_thumb", [
    ("uuid", (str, 36)),
    ("fuid", (str, 36)),
    ("width", int),
    ("height", int),
    ("type", str),
])
folder_entry = gen_table('folder_entry', [
    ('uuid', (str, 36)),
    ('fuid', (str, 36)),
    ('folder', str),
    ('name', str),
])
folder_length = gen_table("folder_length", [
    ('uuid', (str, 36)),
    ('name', str),
    ('len', int),
])
account = gen_table('account', [
    ColumnDef('uuid', (str, 36), primary_key=True),
    ColumnDef('username', str, primary_key=True),
    ColumnDef('type', int, default=0),
    ("email", str),
    ('password', str),
    ('date', int),
    ColumnDef("activated", bool, default=False),

    ColumnDef('foldlen', int, default=0),
    ColumnDef('favslen', int, default=0),
    ColumnDef('apilen', int, default=0),
    ColumnDef('description', str, default=''),
    ColumnDef('banuuid', (str, 36), default=''),
    ColumnDef('banreason', str, default=''),

    ColumnDef('public_favourites', bool, default=False),
    ColumnDef('api_allowed', bool, default=False),
])
account_entries = gen_table('account_entries', [
    ('uuid', (str, 36)),
    ('type', int),
    ('euid', (str, 36)),
])
favourites = gen_table('favourites', [
    ('uuid', (str, 36)),
    ('euid', (str, 36)),
])
apikey = gen_table("apikey", [
    ColumnDef('uuid', str, primary_key=True),
    ("name", str),
    ("user", (str, 36)),
    ('created_at', int),
    ColumnDef('last_used', int, default=0),
    ColumnDef('use_count', int, default=0),
    ('scope', str),
])

def connect(db):
    if db.type not in ['postgres', 'sqlite', 'mysql']:
        raise errors.UnknownDatabaseError(db.type)
    if db.type == "postgres":
        db.type += "ql"
    dtype = db.type
    db.type += "+" + {
        "mysql": "aiomysql",
        "sqlite": "aiosqlite",
        "postgresql": "asyncpg"
    }.get(db.type)
    path = sqlalchemy.engine.url.URL.create(db.type)
    if dtype == "sqlite":
        path = path.set(database=utils.getpath('database.db'))
    else:
        path = path.set(host=db.host, port=db.port, username=db.user, password=db.password, database=db.database)

    global db_engine
    db_engine = sqlalchemy.ext.asyncio.create_async_engine(path)

async def init(db):
    if isinstance(db, dict):
        db = utils.Empty(**db)
    connect(db)

    async with db_engine.begin() as conn:
        await conn.run_sync(db_meta.create_all, checkfirst=True)

    await check_sanity()

async def check_sanity():
    conn = await db_engine.connect()

    async def getall(table, dosort=False):
        stmt = table.select()
        if dosort:
            stmt = stmt.order_by(table.c.date.desc())
        return (await conn.stream(stmt)).mappings()

    expected_files = set()
    expected_folders = set()
    expected_hashes = set()
    expected_accounts = set()

    async for ent in await getall(folder_entry):
        expected_folders.add(ent['uuid'])
        if ent["fuid"]:
            expected_files.add(ent['fuid'])
    async for ent in await getall(account_entries):
        expected_accounts.add(ent['uuid'])
        expected_folders.add(ent['euid'])
    async for ent in await getall(favourites):
        expected_accounts.add(ent['uuid'])
        expected_folders.add(ent['euid'])

    folders = 0
    accounts = 0

    last_accs, last_folds = [], []

    wanted_users, wanted_thumbs = {}, {}
    async for ent in await getall(folder, dosort=True):
        folders += 1
        if len(last_folds) < config.confdat.cache.folder_limit:
            wanted_users[ent["creator"]] = None
            if ent["thumb"]:
                wanted_thumbs[ent["uuid"]] = None
            last_folds.append(dict(ent))
        expected_folders.discard(ent['uuid'])
        expected_accounts.add(ent['creator'])
        if ent['thumb']:
            expected_files.add(ent['thumb'])

    async for ent in await getall(account, dosort=True):
        accounts += 1
        if len(last_accs) < config.confdat.cache.account_limit:
            last_accs.append(dict(ent) | {"password": None})
        if ent["uuid"] in wanted_users and wanted_users[ent["uuid"]] is None:
            wanted_users[ent["uuid"]] = dict(ent) | {"password": None}
        expected_accounts.discard(ent['uuid'])

    async for ent in await getall(folder_thumb):
        if ent["uuid"] in wanted_thumbs and wanted_thumbs[ent["uuid"]] is None:
            wanted_thumbs[ent["uuid"]] = dict(ent)

    for fold in last_folds:
        fold["creator"] = wanted_users[fold["creator"]]
        fold["thumb"] = wanted_thumbs.get(fold["uuid"])
        if fold["thumb"]:
            fold["thumb"]["name"] = fold["name"]

    fsize = 0
    files = 0
    async for ent in await getall(file):
        fsize += ent["size"]
        files += 1
        expected_files.discard(ent['uuid'])
        expected_hashes.add((ent['hash'], ent['size']))

    for field, ln in (
        ("file", expected_files),
        ("folder", expected_folders),
        ("account", expected_accounts),
    ):
        if ln:
            await conn.close()
            raise errors.DatabaseIntegrityError(field, len(ln), ln)

    for ent in expected_hashes:
        if not objects.statobject(ent[0], ent[1]):
            await conn.close()
            raise errors.InvalidObjectHash(ent)

    cache.init_cache(folders, accounts, files, fsize, last_folds, last_accs)
    await conn.close()

def _dostmt(table, key, ident, offpair=None, orderby=None, **morewhere):
    com = table.select().where(getattr(table.c, key) == ident)
    for k, v in morewhere.items():
        com = com.where(getattr(table.c, k) == v)
    if orderby:
        com = com.order_by(getattr(table.c, orderby).desc())
    if offpair:
        com = com.offset(offpair[0]).limit(offpair[1])
    return com

async def get_row(table, key, ident, _conn, one=False, offpair=None, orderby=None, **morewhere):
    conn = _conn or await db_engine.connect()
    com = _dostmt(table, key, ident, offpair, orderby, **morewhere)
    ret = [dict(i) for i in (await conn.execute(com)).mappings().fetchall()]
    if one:
        ret = ret[0] if ret else {}
    if not _conn:
        await conn.close()
    return ret

async def get_rows(table, key, ident, conn, offpair=None, orderby=None, **morewhere):
    com = _dostmt(table, key, ident, offpair, orderby, **morewhere)
    return (await conn.stream(com)).mappings()

async def get_uuid(table, uuid, conn, one=False, offpair=None, **kwargs):
    return await get_row(table, 'uuid', uuid, conn, one=one, offpair=offpair, **kwargs)

async def get_user(username, uuid=False, dopass=False, conn=None):
    d = await get_row(account, "uuid" if uuid else "username", username, conn, True)
    if d and not dopass:
        d.pop("password")
    return d

async def reformat_folder(folder, conn):
    folder = dict(folder)
    folder['creator'] = await get_user(folder["creator"], uuid=True, conn=conn)
    if folder["thumb"]:
        tdata = await get_uuid(folder_thumb, folder["uuid"], conn, True)
        folder["thumb"] = {
            "data": await get_uuid(file, folder["thumb"], conn, True),
            "width": tdata["width"],
            "height": tdata["height"],
            "type": tdata["type"],
            "name": folder["name"],
        }
        folder["thumb"]["data"]["hash"] = folder["thumb"]["data"]["hash"].hex()
    return folder
