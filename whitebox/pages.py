from . import utils, web, user
import time, re, zlib, os

######################################################
statics = {
    r"/main.css": "text/css",
    r"/errors/404.html": None,

    r"/fragments/skeleton.html": None,
    r"/fragments/root.html": None,
    r"/fragments/pre-upload-text.html": None,
    r"/fragments/pre-upload-page.html": None,
    r"/fragments/extra-head.html": None,
    r"/fragments/folder-page.html": None,
    r"/fragments/register-form.html": None,
    r"/fragments/login-form.html": None,
    r"/fragments/ban-form.html": None,
    r"/fragments/search-page.html": None,
    r"/fragments/search-form.html": None,
    r"/fragments/search-result.html": None,
    r"/fragments/upload-info-form.html": None,
    r"/fragments/upload-file-form.html": None,
    r"/fragments/user-pages.html": None,
    r"/fragments/navbar-side-notlogged.html": None,
    r"/fragments/navbar-side-logged.html": None,
    r"/fragments/favourites-page.html": None,
    r"/fragments/page-links.html": None,
    r"/fragments/settings-page.html": None,
    r"/fragments/api-settings.html": None,
    r"/fragments/status-page.html": None,
    r"/fragments/terms-of-service.html": None,
    r"/fragments/terms-of-service-page.html": None,

    r"/fragments/footer-left.html": None,
    r"/fragments/footer-right.html": None,
}

color = {
    '-[status': 'style="color: #aaaaaa"',
    '-[failed': 'style="color: red"',
    '-[skippe': 'style="color: lightgreen"',
}
def _file_upload_renderer_name(name):
    extra = None
    if isinstance(name, tuple):
        name, extra = name
    frag = f'<code {color.get(name[:8], '')}>{utils.xhtml_escape(name[1:])}'
    if not isinstance(extra, list):
        extra = [extra]
    for i in extra:
        frag += "<br>" + utils.xhtml_escape(i or "")
    return frag + "</code>"

def _thumb_render(thumb):
    if not thumb:
        return ''

    w, h = 105, thumb["height"]
    ratio = thumb["width"] / h
    h = w / ratio

    name = utils.url_escape(thumb["name"])
    return f'<a href="/folder/{name}"><img src="/fthumb/{name}.{thumb["type"]}" width="{int(w)}" height="{int(h)}" alt="thumb for {thumb["name"]!r}"/></a>'

tables = {
    "file_upload_entries": {
        "headers": ["#", "Name", "Size", "MIME"],
        "renderers": [
            lambda num: f'<span style="color: #616161">{num}</span>',
            _file_upload_renderer_name,
            None,
            lambda mime: f'<code>{mime}</code>',
        ]
    },
    "file_entries": {
        "headers": ["Name", "Size", "MIME"],
        "renderers": [
            lambda pair: '<a href="/folder/{}/{}" {}>{}</a>'.format(
                utils.url_escape(pair[0]),
                utils.url_escape(pair[1]),
                pair[3] if (len(pair) > 3 and pair[3]) else "",
                utils.xhtml_escape(pair[2] if (len(pair) > 2 and pair[2]) else pair[1]),
            ),
            utils.get_human_size,
            lambda mime: f'<code>{mime}</code>',
        ]
    },
    "folder_results": {
        'headers': ["", "Name", "Date of upload", "Category", "Uploader", "Views/Favs"],
        'renderers': [
            _thumb_render,
            lambda name: f'<a href="/folder/{utils.url_escape(name)}">{utils.xhtml_escape(name)}</a>',
            lambda sec: time.ctime(sec),
            None,
            lambda creator: user.color_user(creator, withlink=True),
            lambda pair: f'{pair[0]} / {pair[1]}',
        ],
        "rowextras": 'style="height: 50px"',
    },
    "user_entries": {
        'headers': ["Name", "Folders", "Date of creation"],
        'renderers': [
            lambda name: user.color_user(name, withlink=True),
            None,
            time.ctime,
        ]
    },
    "api_keys": {
        "headers": ["Name", "Created at", "Last used", "Use count", ""],
        "renderers": [
            None,
            time.ctime,
            lambda date: time.ctime(date) if date > 0 else "[never]",
            None,
            lambda name: f'<a href="/settings/api?revoke={utils.url_escape(name)}">Revoke</a>'
        ]
    },
}

class StaticHandler(web.BaseHandler):
    async def get(self):
        mime, data, date = statics[self.request.path]
        self.set_header("Content-Type", mime)
        self.set_header("Cache-Control", "max-age=31536000, public, immutable")
        self.write(data)

def get_static(path, onlypublic=False):
    data = None
    if istext := path in statics:
        mime = statics[path]
        mime = mime[0] if isinstance(mime, tuple) else mime
        if onlypublic and not mime:
            raise FileNotFoundError(path)
        istext = (not mime) or mime.startswith("text/")
    mode = "r" if istext else "rb"

    if os.access(utils.getpath('pages/' + path), os.R_OK):
        data = open(utils.getpath('pages/' + path), mode).read()
    elif os.access(__file__[:-3] + "/" + path, os.R_OK):
        data = open(__file__[:-3] + "/" + path, mode).read()
    else:
        raise FileNotFoundError(path)

    crcdat = data.encode() if istext else data
    return data, utils.hexcode(zlib.crc32(crcdat).to_bytes(4, byteorder='little', signed=False)).rjust(8, '0')

def init_pages(routes):
    for key in statics:
        data, date = get_static(key)
        statics[key] = (statics[key], data, date)
        if statics[key][0] is not None:
            routes.append((key, StaticHandler))

def indent_fragment(data, indent=4):
    return data.strip().replace('\n', '\n' + ''.join([' ' for _ in range(indent)]))

# python doesn't use german keyword names lol
def get_fragment(fid, indent=8, keys=None, nodiv=False, klasse=False, **kwargs):
    fragment = statics['/fragments/' + fid + '.html'][1]
    keys = keys or kwargs

    reqs = set(re.compile("<<([a-z_]*)>>").findall(fragment))
    neqs = set(re.compile("<<noescape:([a-z_]*)>>").findall(fragment))
    frag = set(re.compile('<<fragment:([a-z_-]*)(:[a-z,]*)?>>').findall(fragment))
    for var in frag:
        fragname = var[0]
        fragargs = {}
        for arg in filter(lambda i: i, var[1][1:].split(',')):
            fragargs[arg] = keys.get(fragname + ':' + arg)
        fragment = fragment \
                .replace(f'<<fragment:{var[0]}{var[1]}>>',
                         get_fragment(fragname, indent=0, keys=fragargs))
    for (var, val) in keys.items():
        reqs.discard(var)
        neqs.discard(var)
        if not isinstance(val, str):
            if isinstance(val, int):
                val = str(val)
            else:
                val = str(val or '')
        fragment = fragment                                     \
                .replace(f'<<{var}>>', utils.xhtml_escape(val)) \
                .replace(f'<<noescape:{var}>>', val)

    for var in reqs.union(neqs):
        fragment = fragment                         \
                .replace(f'<<{var}>>', '')          \
                .replace(f'<<noescape:{var}>>', '')

    div = f'<div id="{fid}"' + (f' class="{klasse}"' if klasse else '') + '>'
    part = fragment if nodiv else f'{div}\n    {fragment}\n</div>'
    return indent_fragment(part, indent=indent)

def build_table_frag(table_def, rows, nodiv=False, klasse=None):
    table = tables[table_def]

    fragment = f'<table class="list t_{table_def}">\n'

    if table.get('headers'):
        fragment += f'    <tr class="t_{table_def}_row">\n'
        for (hn, header) in enumerate(table['headers']):
            fragment += f'        <th class="list t_{table_def}_n{hn}">{utils.xhtml_escape(header)}</th>\n'
        fragment += '    </tr>\n'

    redat = table.get("rowextras", "")
    for row in rows:
        fragment += f'    <tr class="t_{table_def}_row">\n'
        for (num, col) in enumerate(row):
            data = (table["renderers"][num] or (lambda i: i))(col)

            if not isinstance(data, str):
                data = str(data)
            clname = f"t_{table_def}_n{num}"
            fragment += f'        <td class="list {clname}" {redat}>{data}</td>\n'
        fragment += '    </tr>\n'
    fragment += '</table>'

    div = f'<div id="{table_def}"' + (f' class={klasse}' if klasse else '') + '>'
    return fragment if nodiv else f'{div}\n    {fragment}\n</div>'
