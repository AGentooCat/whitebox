from argparse import ArgumentParser as argparser
from . import errors, log

def parse_args(argv):
    from . import VERSION
    parser = argparser(
        prog="whitebox",
        description=f"A self-hostable extensible Internet Archive clone (version {VERSION})"
    )
    parser.add_argument("-c", "--config", default="/etc/whitebox.toml",
                        help="config file to load (default: /etc/whitebox.toml)")
    parser.add_argument('-f', '--foreground', default=False,
                        action="store_true",
                        help="run in foreground (default: false)")
    parser.add_argument('-p', '--pidfile', default=None,
                        help="pid file to store (ignored with -f)")
    parser.add_argument('-l', '--logfile', default=None,
                        help='log file to use')
    parser.add_argument('-L', '--loglevel', default='info',
                        help="log level to use [crit, error, warn, (default=)info, debug]")
    parser.add_argument('-s', '--syslog', default=False,
                        action='store_true',
                        help="use syslog instead (ignored with foreground)")
    args = parser.parse_args(argv[1:])

    if args.loglevel not in log.levels:
        raise errors.UnknownLogLevelError(args.loglevel)

    return args
