import os, importlib.util, logging
from . import errors, utils
log = logging.getLogger(__name__)

plugins = {}

# a plugin:
# 1) subclasses this base
# 2) sets _category
# 3) declares a run(arg) which does something with the arg
# 4) declares a can_run(runarg) which checks if it does something with the arg
#    True for yes, False for no, None for uncertain
# everything else the plugin declares or does is up to the category it uses
#
# if the category is intended to be chain-based rather than one-off-based
# (like redirect site resolvers to normal url downloaders) then the return
# value of run() must be compatible with the rest of the category
# (see run_in_category below)
class BasePlugin:
    _filename = None
    _category = None

    def __init__(self, filename):
        _filename = filename

    def run(self, data):
        raise errors.UndeclaredRunError(self._category, self._filename, self.__name__)
    def can_run(self, data):
        raise errors.UndeclaredCanRunError(self._category, self._filename, self.__name__)

# a plugin file declares plugins as above,
# then sets file-wide "plugins" to a list of plugins to load

def init_plugins():
    builts = __file__.rpartition("/")[0] + "/plugins/"
    extras = utils.getpath("plugins/")
    founds = [(True, builts + "/" + file) for file in os.listdir(builts)]
    if os.access(extras, os.F_OK):
        founds.extend([(False, extras + "/" + file) for file in os.listdir(extras)])

    founds = filter(lambda i: i[1].endswith(".py"),
                    sorted(founds, key=lambda i: i[1].rpartition("/")[2]))
    builn, extln = 0, 0
    for is_builtin, file in founds:
        modname = file.rpartition("/")[2].partition('.')[0]
        spec = importlib.util.spec_from_file_location(modname, file)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)

        if not getattr(mod, "plugins", None):
            log.warning("Plugin-file %r doesn't have its \"plugins\" - set it to a list of plugins in the file. Skipped.", file)
            continue
        for plugin in mod.plugins:
            register_plugin(file, plugin)
        if is_builtin:
            builn += len(mod.plugins)
        else:
            extln += len(mod.plugins)
    log.info("Loaded %d built-in and %d external plugin(s).", builn, extln)

def register_plugin(filename, plugin):
    pluname = plugin.__name__
    if not (catname := plugin._category):
        raise errors.UndeclaredCategoryError(plugin._filename, pluname)

    category = plugins.setdefault(catname, {})
    if oldplugin := category.get(pluname):
        raise errors.DuplicatePluginError(catname, pluname, oldplugin._filename, filename)

    if getattr(plugin, "_lazyinit", False) or getattr(plugin, "_noinit", False):
        plugin._filename = filename
        category[pluname] = plugin
    else:
        category[pluname] = plugin(filename)

# last arg = last plugin's ret or original data
#
# next pick restart
#   no   no      no : break on a none and return last arg
#                     for chain-based plugin categories
#  yes   no      no : continue with the last arg
#                     same as prev but some plugins can reject the input
#  yes  yes      no : break and return when one returns something
#                     for finding a plugin willing to handle the input
#  yes   no     yes : keep running through all plugins until all returns none
#                     for handling (exp) chained "wait 5 secs for link" sites
def run_in_category(category, data, nextonnone=False, pickfirstone=False, restartuntilallnone=False):
    if pickfirstone or restartuntilallnone:
        nextonnone = True
    if pickfirstone and restartuntilallnone:
        raise errors.PickFirstVSRestartUntilNoneError
    if category not in plugins:
        return data
    cplugin = plugins[category]

    while True:
        ret = None
        allnone = True
        for plugin in cplugin.values():
            if getattr(plugin, "_lazyinit", False) and not getattr(plugin, "_noinit", False):
                # was declared as lazyinit
                plugin = plugin(plugin._filename)
            pret = plugin.run(data)
            if pret is None:
                if not nextonnone:
                    ret = data
                    break
                continue
            allnone = False
            data = pret
            if pickfirstone:
                ret = data
                break
        if (nextonnone and allnone) or not ret:
            ret = data
        if not (restartuntilallnone and not allnone):
            break
    return ret

def check_arg_in_category(category, data):
    if category not in plugins:
        return False
    for plugin in plugins[category].values():
        if getattr(plugin, "_lazyinit", False) and not getattr(plugin, "_noinit", False):
            # was declared as lazyinit
            plugin = plugin(plugin._filename)
        # we interpret "None for uncertainty" as "False for no"
        if plugin.can_run(data):
            return plugin
    return None
