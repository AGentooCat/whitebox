import html5lib, cssutils, urllib, re, tornado.httpclient
from . import utils, log, config

def get_css_links(block, inner=False):
    cssutils.log._log = log.nulog
    if inner:
        block = "#self {" + block + "}"
    return set(cssutils.getUrls(cssutils.parseString(block)))

def get_html_links(block, href=False):
    links = []
    tag = str(block.tag)
    if tag.endswith("}style"):
        links.extend(get_css_links(block.text))
    if style := block.attrib.get("style"):
        links.extend(get_css_links("#self { " + style + "}", True))
    if (href or not tag.endswith("}a")) and (thref := block.attrib.get("href")):
        links.append(thref)
    if src := block.attrib.get("src"):
        links.append(src)
    for child in block:
        links.extend(get_html_links(child, href))
    return set(links)

def get_inner_links(resp, data, href=False):
    links = []
    if target := resp.headers.get("Location"):
        links.append(target)
    mime = utils.get_mime(data)
    if mime.startswith("text/html"):
        links += get_html_links(html5lib.parse(data), href)
    elif mime.startswith("text/css"):
        links += get_css_links(data)

    resolved = []
    for link in links:
        if link.startswith("//"):
            link = "http:" + link
        parsed = urllib.parse.urlparse(link)._replace(fragment=None)
        realink = None
        if parsed.netloc:
            realink = parsed.geturl()
        else:
            realink = urllib.parse.urljoin(resp.effective_url, parsed.geturl())
        if parsed.scheme and parsed.scheme not in ["http", "https"]:
            continue
        resolved.append(realink)
    return set(resolved)

def get_proxy(url):
    for matcher in config.confdat.proxy.list:
        if matcher.matches(url):
            return matcher.proxy
    return None

def check_url(url):
    if not url:
        return None
    url = urllib.parse.urlparse(url)
    if not url.hostname or url.scheme not in ['http', 'https']:
        return None

    ipv4_local = re.compile('^' + (r'(\d+)\.' * 3) + r'(\d+)(:\d+)?$')
    if url.hostname == 'localhost':
        return None
    elif match := ipv4_local.findall(url.hostname):
        # test for private networks
        if match[0][0] in ('0', '10', '127', '255'):
            return None

    try:
        _ = url.port
    except ValueError: # urllib.ParseResult raises if port is not integer on request
        return None
    return url

async def geturl(furl, callback=None):
    proxy = get_proxy(furl)
    if not proxy:
        proxy = utils.Empty(hostname=None, port=None)
    request = tornado.httpclient.HTTPRequest(
        url=furl,
        proxy_host=proxy.hostname,
        proxy_port=proxy.port,
        streaming_callback=callback,
        user_agent=config.confdat.upload.user_agent,
    )
    tornado.httpclient.AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient')
    client = tornado.httpclient.AsyncHTTPClient()

    try:
        return await client.fetch(request, raise_error=False)
    except Exception as e:
        return utils.Empty(error=e)
