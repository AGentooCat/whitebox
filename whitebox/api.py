from . import datahands, web, db, config, upload, utils, thread, ipc, url, file as fmod, user as muser
import time, tornado.web, logging, json, base64
log = logging.getLogger(__name__)

class APILogEntry:
    reqs = None
    limit = 0
    block_until = None

    _min_wait_time = None
    def __init__(self, key):
        self.key = key
        self.reqs = []
        self.limit = config.confdat.api.api_limit

    def check429(self):
        now = time.time()
        if self.block_until and self.block_until >= now:
            return int(self.block_until - now)

        self.reqs.append(now)
        if (len(self.reqs) - 1) < self.limit:
            return None

        elapsed = now - self.reqs.pop(0)
        if elapsed < self.limit:
            # overused
            self.reqs = []
            self.block_until = now + config.confdat.api.block_time
            return config.confdat.api.block_time
        return None

apilog = {}
def is_overused(key):
    return apilog.setdefault(key, APILogEntry(key)).check429()

class APIBaseHandler(web.BaseHandler):
    _failed = False
    def _authplz(self):
        self.set_header("WWW-Authenticate", 'Basic realm="API"')
        self._fail("Wrong/Invalid API key (set Authenticate header to the API key)", 401)

    async def _api_check(self):
        if not (api := self.request.headers.get("Authenticate", "")):
            return self._authplz()
        self._apikey = api = api.rpartition(' ')[2]
        async with db.db_engine.begin() as conn:
            key = await db.get_uuid(db.apikey, api, conn, True)
            if not key:
                return self._authplz()
            self.current_user = user = await db.get_uuid(db.account, key["user"], conn, True)
            if not user or user["type"] == muser.UserSType["banned"]:
                return self._authplz()
            if not self.isapi():
                return self._fail("User denied of API access")

            key["last_used"] = int(time.time())
            key["use_count"] += 1
            await conn.execute(db.apikey.update().where(db.apikey.c.uuid == api), key)

        if self.current_user["type"] != muser.UserSType["admin"] and (retry := is_overused(api)):
            retry = str(retry)
            self.set_header("Retry-After", retry)
            return self._fail(f"Reached API limit of this key; retry in {retry} seconds.", 429)

        self._key = key
        self._user = user
        return True

    def _succ(self, data):
        self.set_status(200)
        self.set_header("Content-Type", "application/json")
        self.write(json.dumps({"status": "success", "data": data}))

    def _fail(self, msg, code=400):
        self.set_status(code)
        self.set_header("Content-Type", "application/json")
        self.write(json.dumps({"status": "error", "reason": msg}))
        self._failed = True

    async def _init(self, needed=None, isget=False):
        if not await self._api_check():
            self._failed = True
            return

        if isget and not self.request.arguments:
            self._failed = True
            self.set_status(400)
            return

        heads = self.request.headers
        if needed and not heads.get("Content-Type", "").startswith(needed):
            self._failed = True
            self.set_status(400)
            return

        return True

class APIUserHandler(APIBaseHandler):
    async def get(self):
        if not await self._init(isget=True):
            return

        if not (username := self.get_argument("username")):
            return self._fail("'username' key must be present")
        if not (user := await db.get_user(username)):
            return self._fail("User is not found", 404)

        sdat = {"user": user}
        async with db.db_engine.begin() as conn:
            if self.get_argument("favourites") and user["public_favourites"]:
                sdat["favourites"] = [
                    await db.reformat_folder(await db.get_uuid(db.folder, i["euid"], conn, True), conn)
                    for i in await db.get_row(db.favourites, "uuid", user["uuid"], conn)
                ]
            if self.get_argument("uploads"):
                sdat["uploads"] = await db.get_row(db.folder, "creator", user["uuid"], conn)

        self._succ(sdat)

class APIFolderHandler(APIBaseHandler):
    async def get(self):
        if not await self._init(isget=True):
            return

        if not (name := self.get_argument("name")):
            return self._fail("'name' key must be present")
        files = self.get_argument("files").lower()
        if files and files not in ("yes", "no", "true", "false", "1", "0"):
            return self._fail("'files' key must be boolean")
        files = files in ("yes", "true", "1")

        async with db.db_engine.begin() as conn:
            if not (folder := await db.get_row(db.folder, "name", name, conn, True)):
                return self._fail("Folder not found", 404)
            folder = await db.reformat_folder(folder, conn)
            if files:
                files = await db.get_uuid(db.folder_entry, folder["uuid"], conn)
                for file in files:
                    del file["uuid"]
                    if file["fuid"]:
                        inner = await db.get_uuid(db.file, file["fuid"], conn, True)
                        del inner["name"]
                        del inner["date"]
                        del inner["uuid"]
                        file |= inner
                        file["hash"] = file["hash"].hex()
                    del file["fuid"]
                folder["files"] = files
        return self._succ(folder)

class APIFolderDeleteHandler(APIBaseHandler):
    async def delete(self, name):
        if not await self._init():
            return
        async with db.db_engine.begin() as conn:
            if not (folder := await db.get_row(db.folder, "name", name, conn, True)):
                return self._fail("Folder not found", 404)
            if self._user["uuid"] != folder["creator"]:
                return self._fail(f"Folder is not owned by you ({self._user['username']})", 401)
            await fmod.delete_folder(conn, folder, self._user)
        return self._succ(None)

class APIUploadKicker(APIBaseHandler):
    def post(self):
        return self._fail("Uploads are disabled")

class APIAppendUploadHandler(APIBaseHandler):
    async def prepare(self):
        if not await self._init():
            return
    async def post(self, name):
        if self._failed:
            return
        async with db.db_engine.begin() as conn:
            fentry = await db.get_row(db.folder, "name", name, conn, True)
            if not fentry:
                return self._fail("Folder not found")
            flist = await db.get_uuid(db.folder_entry, fentry["uuid"], conn)

        if fentry["creator"] != self._user["uuid"]:
            return self._fail("You're not the owner of the folder")
        flist = {(((i["folder"] + "/") if i["folder"] else "") + i["name"]): i for i in flist}
        if (inner := self.get_argument("inner")):
            inner = utils.norm(inner)
        try:
            folder = ipc.request_ipc("new_folder", user_token=self._apikey, userdata=self._user, name=name, exist_uuid=fentry["uuid"], exist_list=flist, inner=inner)
        except Exception as e:
            return self._fail(str(e))

        self._succ({"folder_token": folder})

@tornado.web.stream_request_body
class APINewUploadHandler(APIBaseHandler):
    async def prepare(self):
        if not await self._init("application/json"):
            return
        self.limit = (config.confdat.upload.thumb_max_size * 1.5) + 2097152
        self.bytes_read = 0
        self._file = datahands.FileEntry()

    def data_received(self, chunk):
        if self._failed:
            return

        self.bytes_read += len(chunk)

        if self._check_limit():
            self._file.close()
            self.bytes_read = 0
            self.send_error(413)
            self._failed = True
            return

        self._file.write(chunk)

    async def post(self, name):
        if self._failed:
            return

        jdata = self._file.fread()
        self._file.close()
        try:
            jdata = json.loads(jdata)
        except json.decoder.JSONDecodeError:
            return self._fail("JSON failed to decode")

        if not utils.check_str(name):
            return self._fail('Name must be valid')

        if await db.get_row(db.folder, "name", name, None):
            return self._fail('Folder already exists')

        category = jdata.get("category", "uncategorized")
        if (thumb := jdata.get("thumbnail")):
            try:
                thumb = base64.b64decode(thumb)
            except Exception:
                return self._fail("Thumbnail is invalid")
        desc = jdata.get("description")

        try:
            thumb_fent = upload.get_thumb_fent(thumb, name)
            folder = ipc.request_ipc("new_folder", user_token=self._apikey, userdata=self._user, name=name, desc=desc, thumb=thumb_fent, category=category)
        except Exception as e:
            return self._fail(str(e))

        self._succ({"folder_token": folder})

@tornado.web.stream_request_body
class APIFileUploadHandler(APIBaseHandler):
    async def prepare(self):
        if not await self._init():
            return

        self.limit = config.confdat.data.max_upload_size
        self.bytes_read = 0
        log.debug("Incoming API upload from %r", repr(self.request.headers['user-agent']))
        self._ftoken = self.get_argument("folder_token")
        self._folder = self._get_folder(usethis=(self._ftoken, self._apikey), redirect=False)
        if not self._folder:
            return APIBaseHandler._fail(self, "Invalid folder/API token")

        self._file = datahands.FileEntry()

        ipc.request_ipc("pause_folder", token=self._ftoken, user_token=self._apikey, do_pause=True)
        self.find = ipc.request_ipc("new_file", token=self._ftoken, user_token=self._apikey, init_msg="api upload in progress")

    def data_received(self, chunk):
        if self._failed:
            return

        self.bytes_read += len(chunk)

        if self._check_limit():
            self._file.close()
            self.bytes_read = 0
            self.send_error(413)
            self._failed = True
            return

        self._file.write(chunk)
        ipc.request_ipc("size_file", token=self._ftoken, user_token=self._apikey, find=self.find, size=self.bytes_read)

    def _fail(self, msg, code=400):
        ipc.request_ipc("pause_folder", token=self._ftoken, user_token=self._apikey, do_pause=False)
        ipc.request_ipc("set_fmsg", token=self._ftoken, user_token=self._apikey, msg=msg)
        if self.find is not None:
            ipc.request_ipc("pop_find", token=self._ftoken, user_token=self._apikey, find=self.find)
            self.find = None

        return APIBaseHandler._fail(self, msg, code=code)

    async def post(self, name):
        if self._failed:
            return
        if not name:
            self._file.close()
            return self._fail("Invalid filename")

        folder = self._folder
        token = self._ftoken

        isarc = bool(self.get_argument("archive"))
        self._file.name = name
        self._file.finish()

        finds = await thread.run_in_thread(datahands.submitfiles, token, self._apikey, [self._file], isarc=isarc)
        if isinstance(finds, str):
            return self._fail(finds)
        ipc.request_ipc("pop_find", token=self._ftoken, user_token=self._apikey, find=self.find)

        self._succ({"folder": {"name": folder["name"]}, "finds": finds})

@tornado.web.stream_request_body
class APINewURLHandler(APIBaseHandler):
    def _fail(self, msg, code=400):
        ipc.request_ipc("pause_folder", token=self._ftoken, user_token=self._apikey, do_pause=False)
        ipc.request_ipc("set_fmsg", token=self._ftoken, user_token=self._apikey, msg=msg)
        if self.find is not None:
            ipc.request_ipc("pop_find", token=self._ftoken, user_token=self._apikey, find=self.find)
            self.find = None

        return APIBaseHandler._fail(self, msg, code=code)

    async def prepare(self):
        if not await self._init("application/json"):
            return
        self.limit = 1048576 * 5
        self.bytes_read = 0

        self._ftoken = self.get_argument("folder_token")
        self._folder = self._get_folder(usethis=(self._ftoken, self._apikey), redirect=False)
        if not self._folder:
            return APIBaseHandler._fail(self, "Invalid folder/API token")

        self._file = datahands.FileEntry()

        ipc.request_ipc("pause_folder", token=self._ftoken, user_token=self._apikey, do_pause=True)
        self.find = ipc.request_ipc("new_file", token=self._ftoken, user_token=self._apikey, init_msg="api upload in progress")

    def data_received(self, chunk):
        if self._failed:
            return

        self.bytes_read += len(chunk)

        if self._check_limit():
            self._file.close()
            self.bytes_read = 0
            self.send_error(413)
            self._failed = True
            return

        self._file.write(chunk)

    def post(self):
        if self._failed:
            return

        jdata = self._file.fread()
        self._file.close()
        try:
            jdata = json.loads(jdata)
        except json.decoder.JSONDecodeError:
            return self._fail("JSON failed to decode")

        if not ("urls" in jdata
                and isinstance(jdata["urls"], list)
                and jdata["urls"]
                and all(isinstance(i, str) for i in jdata["urls"])):
            return self._fail('"urls" key must be present as a list of strings')

        files = []
        for eurl in jdata["urls"]:
            if not (rurl := url.check_url(eurl)):
                return self._fail("Please submit a valid URL.")
            files.append(rurl.geturl())
        jdata["urls"] = files
        files = []
        for rurl in jdata["urls"]:
            find = ipc.request_ipc("new_file", token=self._ftoken, user_token=self._apikey, init_msg="downloading: " + rurl)
            datahands.FileEntry.from_url(rurl, [self._ftoken, self._apikey, find])
            files.append(find)

        ipc.request_ipc("pop_find", token=self._ftoken, user_token=self._apikey, find=self.find)
        self._succ({"folder": {"name": self._folder["name"]}, "finds": files})

class APICloseFolderHandler(APIBaseHandler):
    async def prepare(self):
        if not await self._init():
            return

        self._ftoken = self.get_argument("folder_token")
        self._folder = self._get_folder(usethis=(self._ftoken, self._apikey), redirect=False)
        if not self._folder:
            return APIBaseHandler._fail(self, "Invalid folder/API token")

    def post(self):
        if self._failed:
            return

        ipc.request_ipc("close", token=self._ftoken, user_token=self._apikey, foldat=None)

        self._succ(None)

class APIFinishFolderHandler(APIBaseHandler):
    async def prepare(self):
        if not await self._init():
            return

        self._ftoken = self.get_argument("folder_token")
        self._folder = self._get_folder(usethis=(self._ftoken, self._apikey), redirect=False)
        if not self._folder:
            return APIBaseHandler._fail(self, "Invalid folder/API token")

    async def post(self):
        if self._failed:
            return

        comdata = ipc.request_ipc("commit", token=self._ftoken, user_token=self._apikey)
        if isinstance(comdata, str):
            return self._fail(comdata)

        fentry = await upload.commit_folder(comdata)
        ipc.request_ipc("close", token=self._ftoken, user_token=self._apikey, foldat=fentry, existing=bool(comdata.existing))

        if inner := self._folder["inner"] or "":
            inner = "/" + inner
        self._succ({"redirect_url": "/folder/" + self._folder["name"] + inner})
