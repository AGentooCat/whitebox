import tornado.ioloop, functools, asyncio, logging, threading
from . import config
log = logging.getLogger(__name__)

threads = set()

def cancel_threads():
    for tid in threads:
        if "cancel" in dir(tid):
            # async based
            tid.cancel()
        # normal threads can't be closed
    threads.clear()

async def run_in_thread(func, *args, **kwargs):
    while len(threads) >= config.confdat.process.thread_limit:
        await asyncio.sleep(1)

    tid = tornado.ioloop.IOLoop.current().run_in_executor(None, functools.partial(func, *args, **kwargs))
    threads.add(tid)
    try:
        ret = await tid
        threads.remove(tid)
    except asyncio.exceptions.CancelledError:
        # the process is going down so we cancelled all thread-futures
        while True:
            await asyncio.sleep(10)
    return ret

async def spawn_thread(name, target, *args, **kwargs):
    while len(threads) >= config.confdat.process.thread_limit:
        await asyncio.sleep(1)
    tid = threading.Thread(name=name, target=target, args=args, kwargs=kwargs)
    tid.start()
    threads.add(tid)
    return tid

def join(tid):
    r = tid.join()
    threads.remove(tid)
    return r
