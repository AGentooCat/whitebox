class WBoxException(Exception):
    fmtstr = None
    # args = stdlib.Exception sets it

    def __str__(self):
        return self.fmtstr % self.args

class UnrecognizedArchiveError(WBoxException):
    fmtstr = "Invalid archive type: %r"

class ConfigError(WBoxException):
    pass

class WrongKeyTypeError(ConfigError):
    fmtstr = "Wanted %s for key %r; got %s"

class MissingKeyError(ConfigError):
    fmtstr = "Missing key %r in config"

class NoBasePathError(WBoxException):
    fmtstr = "Please call whitebox.config.parse_config first!"

class NoDatabaseError(WBoxException):
    fmtstr = "Please call whitebox.db.init or connect first!"

class UnknownDatabaseError(WBoxException):
    fmtstr = "Unknown database type %r"

class DatabaseIntegrityError(WBoxException):
    fmtstr = "Integrity Error: Database references nonexistent %s UUIDs (count:%d) (%r)"
class InvalidObjectHash(WBoxException):
    fmtstr = "Integrity Error: file references nonexistent/incorrect object hash %r"

class UnknownLogLevelError(WBoxException):
    fmtstr = "Unknown loglevel %r"

class OverflowingProxyError(WBoxException):
    fmtstr = "Invalid proxy spec: %r"

class HashCollisionError(WBoxException):
    fmtstr = "Two different files have the same Blake2b hash.\nThe hash in question: 0x%s\nThe file WhiteBox previously received: %s\nThe file that has the different data and same hash, saved for examination: %s"

class PluginError(WBoxException):
    pass

class DuplicatePluginError(PluginError):
    fmtstr = "Plugin '%s.%s' is in both files %r and %r."

class UndeclaredRunError(PluginError):
    fmtstr = "Plugin '%s.%s' doesn't have a run(arg)!"
class UndeclaredCanRunError(PluginError):
    fmtstr = "Plugin '%s.%s' doesn't have a can_run(arg)!"

class UndeclaredCategoryError(PluginError):
    fmtstr = "Plugin '%s.%s' doesn't have a _category set!"

class PickFirstVSRestartUntilNoneError(PluginError):
    fmtstr = "pickfirstone and restartuntilallnone of plugins.run_in_category clash in logic."

class InvalidFolderError(WBoxException):
    fmtstr = "Can't create this folder: %s"

class InvalidFilenameError(WBoxException):
    fmtstr = "Can't place this file %r: %s"

class BrokenFormError(WBoxException):
    fmtstr = "Unable to handle the formdata: %s"

class InvalidThumbnailError(WBoxException):
    fmtstr = "Thumbnail can't be accepted: %s"
