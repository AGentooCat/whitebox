from . import ipc, utils, config, objects, url, plugin, thread, errors, log as logger
import tornado.ioloop, tornado.httpclient, functools, logging, zipfile
import os, math, tarfile, zipstream, zlib, hashlib, urllib, asyncio
log = logging.getLogger(__name__)

class Chunkifier:
    stream = None
    stop = False
    size = None

    def __init__(self, stream, chksize=1048576, start=None, end=None, compress=False):
        self.stream = stream
        self.chksize = chksize
        self.compress = compress
        if start is not None:
            self.stream.seek(start)
            if end is not None:
                self.size = end - start
    def __iter__(self):
        return self
    def __next__(self):
        if self.stop:
            raise StopIteration

        if self.size is not None:
            size = min(self.chksize, self.size)
            data = self.stream.read(size)
            self.size -= size
        else:
            data = self.stream.read(self.chksize)

        if len(data) == 0:
            # intentional sending of len(0) chunk:
            # chunked responses end with one
            self.stream.close()
            self.stream = None
            self.stop = True
        elif self.compress:
            data = zlib.compress(data, level=9, wbits=31)

        return hex(len(data))[2:].encode() + b'\r\n' + data + b'\r\n'

    def close(self):
        if self.stream:
            self.stream.close()

class SplitChunkifier:
    stream = None
    ranges = None
    stop = False
    current = None

    def __init__(self, stream, ranges, mime, size, chksize=1048576):
        self.chksize = chksize
        self.stream = stream
        self.ranges = ranges
        self.boundary = utils.random_str(32)
        self._boundary = lambda start, end: (\
            '--' + self.boundary + '\r\n' + \
            f'Content-Type: {mime}\r\n' +
            f'Content-Range: bytes {start}-{end - 1}/{size}\r\n' + \
            '\r\n').encode()
        self.current = {'index': 0, 'pos': 0, 'rem': None}

    def pretend(self):
        length = 0
        for part in self.ranges:
            length += len(self._boundary(part[0], part[1]))
            length += part[1] - part[0]
            length += 2
        length += len(self.boundary) + 6 # "--" bound "--\r\n"
        return length

    def __iter__(self):
        return self
    def __next__(self):
        if self.stop:
            raise StopIteration

        buf = b''
        curr = self.ranges[self.current['index']]
        if self.current['pos'] == 0:
            if self.current['index'] != 0:
                buf += b'\r\n'
            buf += self._boundary(curr[0], curr[1])
            self.current['rem'] = curr[1] - curr[0]
            self.stream.seek(curr[0])

        size = min(self.chksize, self.current['rem'])
        buf += self.stream.read(size)
        self.current['rem'] -= size
        self.current['pos'] += size

        if self.current['rem'] == 0:
            ind = self.current['index'] + 1
            if ind == len(self.ranges):
                buf += b'\r\n--' + self.boundary.encode() + b'--\r\n'
                self.stop = True
                self.stream.close()
                self.stream = None
                return buf

            self.current['index'] = ind
            self.current['pos'] = 0

        return buf
    def close(self):
        if self.stream:
            self.stream.close()

def gen_pagelinks(rows, page, size=None, fullsize=None, extra=None):
    rlen = fullsize or len(rows)
    page = max(page, 1)
    if not len(rows) and page == 1:
        return "Page 0/0"

    size = min(size or config.confdat.web.page_size, config.confdat.web.page_size)
    dsize = f"&size={size}" if size != config.confdat.web.page_size else ""

    pagecount = "[...]" if fullsize == -1 else math.ceil(rlen / size)
    off = (page - 1) * size
    extra = extra + '&' if extra else ''

    if (not rows if fullsize == -1 else off >= rlen):
        tprev = ''
        if fullsize > -1:
            tprev = f' | <a href="?{extra}page={pagecount}{dsize}">Prev</a>'
        return f'<a href="?{extra}page=1{dsize}">First</a>{tprev} | Page [out of bounds]/{pagecount}'

    grayout = lambda text: f'<span style="color: #888888">{text}</span>'
    first, prev, tnext, last = [grayout(i) for i in ("First", "Prev", "Next", "Last")]
    last = " | " + last
    if page > 1:
        prev = f'<a href="?{extra}page={page - 1}{dsize}">Prev</a>'
    if page > 2:
        first = f'<a href="?{extra}page=1{dsize}">First</a>'
    if (len(rows) == size + 1 if fullsize == -1 else page < pagecount):
        tnext = f'<a href="?{extra}page={page + 1}{dsize}">Next</a>'

    if fullsize == -1:
        last = ''
    elif page < pagecount - 1:
        last = f' | <a href="?{extra}page={pagecount}{dsize}">Last</a>'

    if fullsize == -1 and len(rows) < size + 1:
        pagecount = page
    linkfrag = f'Page {page}/{pagecount}'
    return f'{first} | {prev} | {linkfrag} | {tnext}{last}'

class WrappedZip:
    stream = None
    istream = None
    def __init__(self, cmp):
        cfmt = zipstream.ZIP_DEFLATED if cmp else zipstream.ZIP_STORED
        clvl = 9 if cmp else None
        self.stream = zipstream.ZipStream(compress_type=cfmt, compress_level=clvl, sized=not cmp)
    def add(self, name, fent):
        if self.istream:
            return
        if fent:
            self.stream.add_path(objects.getobjname(fent["hash"]), arcname=name)
        else:
            self.stream.add(None, name + "/")
    def _nextent(self, size=8192):
        try:
            return next(self.istream)
        except StopIteration:
            return
    def read(self, size=8192):
        if not self.istream:
            self.istream = iter(self.stream)
        return utils.fillbuf(size, self._nextent)
    def close(self):
        for _ in self.istream:
            pass
    def len(self):
        return len(self.stream)

class WrappedTar:
    files = None
    cfile = None
    cmp = False
    size = False
    def __init__(self, cmp=False):
        self.cmp = cmp
        self.files = []
        self.size = 0
    def add(self, name, fent):
        ent = tarfile.TarInfo(name)
        if fent:
            path = objects.getobjname(fent["hash"])
            sret = os.stat(path)
            ent.size = sret.st_size
            self.size += ent.size
            req = 512 - (ent.size % 512)
            if req < 512:
                self.size += req
        else:
            path = None
            ent.name += '/'
            ent.type = tarfile.DIRTYPE
        self.size += len(ent.tobuf(format=tarfile.PAX_FORMAT))
        self.files.append((path, ent))
    def _cmp(self, buf):
        if self.cmp:
            return zlib.compress(buf, level=9, wbits=31)
        return buf
    def read(self, size=8192):
        buf = utils.fillbuf(size, self._nextent)
        return self._cmp(buf) if buf else b''
    def _nextent(self, size):
        if self.cfile:
            buf = self.cfile.read(size)
            if not buf:
                pos = self.cfile.tell()
                req = 512 - (pos % 512)
                if req < 512:
                    buf = bytes(req)
                self.cfile.close()
                self.cfile = None
            if buf or not self.files:
                return buf
        if len(self.files) == 0:
            return b''
        cfile = self.files.pop(0)
        if cfile[0]:
            self.cfile = open(cfile[0], "rb")
        return cfile[1].tobuf(format=tarfile.PAX_FORMAT)
    def close(self):
        if self.cfile:
            self.cfile.close()
    def len(self):
        if self.cmp:
            raise TypeError
        return self.size

class FileEntry:
    uuid = None
    size = None
    mime = None
    name = None

    origin = None
    pending = None
    failmsg = None
    skipthis = None

    _backing_file = None
    _hash = None
    _finished = False

    def __init__(self, name=None, dofile=True):
        self.uuid = utils.get_uuid()
        self.size = 0
        self.name = name
        self.origin = "[[uploaded]]"
        self.pending = None
        self._finished = False

        if dofile:
            self._backing_file = utils.get_temp()
            self._hash = hashlib.blake2b()

    def fread(self):
        if self._backing_file is None:
            return
        if self._finished:
            return open(self._backing_file, "rb").read()
        self.update_mime()
        self._backing_file.seek(0)
        return self._backing_file.read()

    def write(self, data):
        if self._backing_file is not None and not self._finished:
            self._backing_file.write(data)
            self._hash.update(data)
            self.size += len(data)

    def flush(self):
        if self._backing_file is not None and not self._finished:
            self._backing_file.flush()

    def finish(self):
        if self._finished:
            return
        name = self._backing_file.name
        self._backing_file.close()
        self._backing_file = name
        self._hash = self._hash.digest()
        self._finished = True
        self.update_mime()

    def close(self):
        if self._backing_file is None:
            return
        if self._finished:
            utils.remove(self._backing_file)
            return
        utils.remove(self._backing_file.name)
        self._backing_file.close()
        self._backing_file = None

    def update_mime(self):
        if self._backing_file is None:
            return
        if self._finished:
            self.mime = utils.get_mime(self._backing_file)
            return
        self.flush()
        self.mime = utils.get_mime(self._backing_file.name)

    def from_file(file, name=None):
        if (isname := isinstance(file, str)):
            # is a path
            file = open(file, "rb")
        fname = file.name

        self = FileEntry(utils.norm(name or fname.rpartition("/")[2]))
        dest = self._backing_file.name
        self.close()
        os.rename(fname, dest)
        self._backing_file = open(dest, "rb")
        self.update_mime()
        while buf := file.read(0x10000):
            self._hash.update(buf)
            self.size += len(buf)
        self.finish()

        if isname:
            file.close()
        else:
            file.seek(0)
        return self

    def from_folder(folder):
        files = []
        for file in os.fwalk(folder):
            file = list(file)
            abspath = file[0]
            file[0] = utils.norm(abspath[len(folder):])
            for subfile in file[2]:
                files.append(FileEntry.from_file(abspath + "/" + subfile, file[0] + "/" + subfile))
        return files

    def from_url(url, folder, done=None):
        self = FileEntry()
        done = done or set()
        self.pending = "downloading: " + url
        self.origin = url
        f = asyncio.get_running_loop().create_task(self._from_url(folder, done))
        tornado.ioloop.IOLoop.current().add_future(f, lambda _: True)
        return self

    async def _from_plugin(self, folder, pluhand):
        pluname = type(pluhand).__name__
        lbuf = []
        def do_flog(folder, lbuf, line):
            lbuf.append(line)
            if len(lbuf) > 5:
                lbuf = lbuf[-5:]
            ipc.request_ipc("msg_file", token=folder[0], user_token=folder[1], find=folder[2], msg=(f"handling with plugin {pluname}: {self.origin}", lbuf))
        loghand = logger.getFuncLogger(pluname + "Logger", ffunc=functools.partial(do_flog, folder, lbuf))
        ipc.request_ipc("msg_file", token=folder[0], user_token=folder[1], find=folder[2], msg=f"handling with plugin {pluname}: {self.origin}")

        fents = []
        try:
            fents = await thread.run_in_thread(pluhand.run({
                "url": self.origin,
                "logger": loghand,
            }))
            plzwait = None
        except Exception as e:
            plzwait = e

        if not self._backing_file or not ipc.request_ipc("valid_find", token=folder[0], user_token=folder[1], find=folder[2]):
            if self._backing_file:
                self.close()
            for fent in fents:
                fent.close()
            return

        if plzwait:
            ipc.request_ipc("fail_file", token=folder[0], user_token=folder[1], find=folder[2], msg=f"plugin {pluname} failed: {plzwait!s}")
            self.close()
            return

        initial = "url_import/plugin/" + pluname
        if isinstance(fents, tuple):
            initial, fents = fents
        if len(fents) == 0:
            ipc.request_ipc("fail_file", token=folder[0], user_token=folder[1], find=folder[2], msg="no files were downloaded")
            self.close()
            return

        for fent in fents:
            fent.name = initial + "/" + fent.name
            fent.finish()
        ipc.request_ipc("add_files", token=folder[0], user_token=folder[1], files=fents)
        ipc.request_ipc("skip_file", token=folder[0], user_token=folder[1], find=folder[2], msg="see files with the dir: " + fents[0].name.rpartition("/")[0])
        self.pending = None
        self.close()

    async def _from_url(self, folder, done):
        # hack to get add_future above return back to POST handler
        _ = await asyncio.sleep(0)

        furl = self.origin
        if plugin.check_arg_in_category("PreURLHandler", furl):
            furl = await thread.run_in_thread(
                plugin.run_in_category, "PreURLHandler", furl,
                restartuntilallnone=True
            )
        self.origin = furl

        if (pluhand := plugin.check_arg_in_category("URLDownloader", furl)):
            return await self._from_plugin(folder, pluhand)

        ret = await url.geturl(furl, callback=functools.partial(self._data_call, folder))

        if not self._backing_file or not ipc.request_ipc("valid_find", token=folder[0], user_token=folder[1], find=folder[2]):
            return self.close()

        self.update_mime()
        self.finish()
        if ret.error:
            ipc.request_ipc("fail_file", token=folder[0], user_token=folder[1], find=folder[2], msg=repr(ret.error))
            self.close()
            return

        name = None
        dirpath = urllib.parse.urlparse(ret.effective_url)
        if dispo := ret.headers.get("Content-Disposition"):
            name = utils.parse_header_line("Content-Disposition: " + dispo)[2].get("filename")
        if not name:
            name = utils.url_descape(dirpath.path.rpartition("/")[2]) or "index.html"

        dirpath = dirpath.scheme + "/" + dirpath.netloc + "/" + dirpath.path.rpartition("/")[0]
        self.name = utils.norm("url_import/" + dirpath + "/" + name)
        self.pending = None

        if self.mime in ("text/html", "text/css"):
            for link in filter(lambda lnk: lnk not in done, url.get_inner_links(ret, self.fread())):
                done.add(link)
                nfold = ipc.request_ipc("new_file", token=folder[0], user_token=folder[1], init_msg="downloading: " + link)
                if nfold is None:
                    break # folder closed
                FileEntry.from_url(link, [folder[0], folder[1], nfold], done)

        ipc.request_ipc("set_file", token=folder[0], user_token=folder[1], find=folder[2], file=self)

    def _data_call(self, folder, data):
        if not self._backing_file:
            return
        if self.size + len(data) > config.confdat.data.max_upload_size:
            ipc.request_ipc("fail_file", token=folder[0], user_token=folder[1], find=folder[2], msg="denied due to upload limits")
            self.close()
            return
        self.write(data)

def get_archive_files(data):
    mime = utils.get_mime(data)
    if mime not in ("application/x-tar", "application/zip"):
        raise errors.UnrecognizedArchiveError(mime)
    name = data
    if isinstance(data, bytes):
        fd = utils.get_temp()
        fd.write(data)
        name = fd.name
        fd.close()

    def addto(files, name, stream):
        file = FileEntry(name)
        while buf := stream.read(4096):
            file.write(buf)
        file.update_mime()
        file.finish()
        if (ofile := files.get(file.name)):
            ofile.close()
        files[file.name] = file

    files = {}
    if mime == 'application/x-tar':
        arc = tarfile.TarFile(name)
        for file in filter(lambda i: i.isfile(), arc.getmembers()):
            addto(files, file.name, arc.extractfile(file))
    elif mime == 'application/zip':
        arc = zipfile.ZipFile(name)
        for file in filter(lambda i: not i.is_dir(), arc.infolist()):
            addto(files, file.filename, arc.open(file))

    utils.remove(name)
    return files.values()

class StreamedFormHandler:
    finished = False

    _backing_file = None
    _offsets = None
    boundary = None
    _boundlen = None

    _blen = 0
    _offset = 0
    _prepend = None
    def __init__(self, content_type, bufmax=4096):
        boundary = content_type.get("boundary")
        if not boundary:
            raise errors.BrokenFormError("boundary not set")

        self.boundary = ('\r\n--' + boundary).encode()
        self._boundary = ('--' + boundary).encode()
        self._boundlen = len(self.boundary)
        self._backing_file = utils.get_temp()
        self._offsets = []
        self._prepend = b'\r\n'
        self.keys = {}
        self.files = []

    def close(self):
        utils.remove(self._backing_file.name)
        self._backing_file.close()

    def close_files(self):
        for file in self.files:
            file.close()

    def update(self, data):
        if self.finished:
            return
        off = self._backing_file.tell()
        self._backing_file.write(data)
        pre = self._prepend or b''
        pl = len(pre)
        tl = len(data) + pl
        data = pre + data
        self._prepend = None

        while data:
            try:
                ind = data.index(b'\r')
            except ValueError:
                break
            if (tl - ind) < self._boundlen:
                self._prepend = data[ind:]
                break

            data = data[ind:]
            tl -= ind
            off += ind
            if data[:self._boundlen] != self.boundary:
                data = data[1:]
                tl -= 1
                off += 1
                continue

            # we got one!
            self._offsets.append(off - len(pre) + 2)
            data = data[self._boundlen:]
            tl -= self._boundlen
            off += self._boundlen

    def finish(self):
        if not self._offsets:
            self.close()
            raise errors.BrokenFormError("no formdata entries")

        self._backing_file.seek(self._backing_file.tell() - 4)
        lfour = self._backing_file.read(4)
        flen = self._backing_file.tell()
        smsg = "lfour:%r, offsets:%r, tell:%d, blen:%d" % (lfour, self._offsets, flen, self._boundlen)
        if lfour != b"--\r\n" or (self._backing_file.tell() - self._boundlen - 2) != self._offsets[-1]:
            # "\r\n--" not covered
            self.close()
            raise errors.BrokenFormError("not proper formdata eof: " + smsg)
        self.finished = True
        # now lets actually go through
        for (n, o) in enumerate(self._offsets[:-1]):
            self._backing_file.seek(o)
            heads, used = utils.get_multipart_heads(self._backing_file, self._boundary)
            if n + 1 < len(self._offsets):
                rl = self._offsets[n + 1] - o - used - 2
            else:
                rl = flen - used - self._boundlen - 4
            if not (dispo := heads.get("Content-Disposition")):
                self.close()
                _ = [i.close for i in self.files]
                raise errors.BrokenFormError("no Content-Disposition in part headers")
            if (fname := dispo.get("filename")):
                file = FileEntry(fname)
                while buf := self._backing_file.read(min(4096, rl)):
                    rl -= len(buf)
                    file.write(buf)
                file.update_mime()
                file.finish()
                self.files.append(file)
            elif (fname := dispo.get("name")):
                key, val = fname, self._backing_file.read(rl)
                self.keys.setdefault(key, []).append(val)
            else:
                self.close()
                _ = [i.close for i in self.files]
                raise errors.BrokenFormError("invalid Content-Disposition in part headers")
        self.keys = dict((k, v[0] if len(v) == 1 else v) for k, v in self.keys.items())
        self.close()

def submitfiles(token, utoken, flist, isarc=False):
    files = []
    if isarc:
        for file in flist:
            fmsg = None
            try:
                files.extend(get_archive_files(file._backing_file))
            except errors.UnrecognizedArchiveError as e:
                fmsg = str(e)
            except Exception as e:
                fmsg = "Invalid archive format " + str(e)
            if fmsg:
                _ = [i.close() for i in files]
                return fmsg
    else:
        files = flist

    if files:
        try:
            return ipc.request_ipc("add_files", token=token, user_token=utoken, files=files)
        except errors.InvalidFilenameError as e:
            _ = [i.close() for i in files]
            return str(e)
    return []
