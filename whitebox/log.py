import syslog, logging, sys
from . import utils

levels = {
    "crit": logging.CRITICAL,
    "critical": logging.CRITICAL,
    "error": logging.ERROR,
    "warn": logging.WARNING,
    "warning": logging.WARNING,
    "info": logging.INFO,
    "debug": logging.DEBUG,
}

logfd = None
nulog = None
loglevel = levels["info"]

def setlevel(level):
    global loglevel
    # level was sanitized by parse_args
    loglevel = levels[level.lower()]
    if logfd:
        logfd.info('Log level set to %s', level)
        logfd.setLevel(loglevel)
        logfd.handlers[0].setLevel(loglevel)

class WrappedSysLogHandler(logging.StreamHandler):
    def __init__(self, ident, opts, facility):
        logging.Handler.__init__(self)
        syslog.openlog(ident, opts, facility)
        self.stream = utils.Empty(
            write=lambda s: syslog.syslog(s),
            flush=lambda: None
        )
class FILE2Log:
    stream = None
    linebuf = None
    level = None
    def __init__(self, logfd, level):
        self.stream = logfd
        self.linebuf = ''
        self.level = levels[level]
    def write(self, text):
        for c in text:
            if c == '\n':
                self.stream.log(self.level, self.linebuf)
                self.linebuf = ''
            else:
                self.linebuf += c
    def flush(self): ...

class FuncHandler(logging.Handler):
    def _init(rfunc=None, ffunc=None):
        self = FuncHandler()
        self.rfunc = rfunc
        self.ffunc = ffunc
        return self

    def emit(self, record):
        if self.rfunc:
            self.rfunc(record)
        if self.ffunc:
            self.ffunc(self.format(record))

def getFuncLogger(name, ffunc=None, rfunc=None):
    logger = logging.getLogger(name + utils.random_str())
    hand = FuncHandler._init(rfunc=rfunc, ffunc=ffunc)
    hand.setLevel(logging.DEBUG)
    logger.addHandler(hand)
    logger.setLevel(logging.DEBUG)
    return logger

def openlog(file):
    fmt = "[%(filename)s][%(levelname)s] [%(process)d:%(asctime)s] %(message)s"
    if file is None: # syslog; it will handle pid+time
        fmt = "[%(filename)s][%(levelname)s] %(message)s"
    fmt = logging.Formatter(fmt, datefmt="%Y-%b-%d %H:%M:%S")
    hand = None
    if isinstance(file, str):
        hand = logging.FileHandler(file, 'a')
    elif file is None:
        hand = WrappedSysLogHandler("whitebox", syslog.LOG_PID | syslog.LOG_NDELAY, syslog.LOG_DAEMON)
    else:
        hand = logging.StreamHandler(file)
    hand.setFormatter(fmt)
    hand.setLevel(loglevel)

    global nulog
    nulog = logging.getLogger("nulog")
    nulog.addHandler(logging.NullHandler())
    nulog.setLevel(loglevel)

    global logfd
    logfd = logging.getLogger()
    logfd.addHandler(hand)
    logfd.setLevel(loglevel)

    sys.stdout = FILE2Log(logfd, "info")
    sys.stderr = FILE2Log(logfd, "error")

    logfd.info("Log file opened")
