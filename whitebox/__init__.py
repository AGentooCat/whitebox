from . import config, args, db, web, log, utils, user, plugin, thread, process
import asyncio, os, sys, time, signal, traceback, tornado.netutil, pwd, grp

VERSION = "v0.0.8"

signalled = 0
def sighand(signum, stack):
    global signalled
    if signum in (signal.SIGINT, signal.SIGTERM):
        txt = "INT" if signum == signal.SIGINT else "TERM"
        if signalled >= 1:
            print(f"Received SIG{txt} a second time; hard-exiting")
            os._exit(0)
        print(f"Received SIG{txt}; exiting")
        if db.db_engine.dispose:
            asyncio.get_running_loop().create_task(db.db_engine.dispose())
        thread.cancel_threads()
        signalled = 1
        sys.exit(0)
    elif signum in (signal.SIGUSR1,):
        print("Received SIGUSR1; printing stacktrace:\n" + ''.join(traceback.format_stack(stack)))

async def do_dbinit(argv):
    await db.init(argv.config.database)

    admin = await db.get_row(db.account, "type", user.UserSType["admin"], None)
    if not admin:
        print('> No admin account found; creating one...')
        pword = utils.random_str()
        passd = utils._passcode(pword)
        admin = {
            'uuid': utils.get_uuid(),
            'username': 'admin',
            'password': passd,
            'date': time.time(),
            'type': user.UserSType["admin"],
            "activated": True,
            "email": "admin@localhost",
        }
        async with db.db_engine.begin() as conn:
            await conn.execute(db.account.insert().values(admin))
        print(f'> "admin" account created with password "{pword}"')
    del admin

def main(argv=None):
    try:
        argv = args.parse_args(argv or sys.argv)
    except Exception as e:
        print(f'Could not parse arguments: {e}')
        return 1
    try:
        argv.config = conf = config.parse_config(argv.config)
    except Exception as e:
        print(f'Could not parse config: {e}')
        return 1

    try:
        suser = pwd.getpwnam(argv.config.data.user)
        sgroup = grp.getgrnam(argv.config.data.group)
    except KeyError:
        print(f"User/Group {argv.config.data.user}/{argv.config.data.group} doesn't exist!")
        return 1
    root = utils.getpath(".")
    if not os.access(root, os.R_OK | os.W_OK):
        utils.mkdir(root)
        os.chmod(root, suser.pw_uid, sgroup.pw_gid)
    os.setgid(sgroup.gr_gid)
    os.setuid(suser.pw_uid)

    if argv.pidfile:
        argv.config.data.pidfile = argv.pidfile
    if argv.logfile:
        argv.config.data.logfile = argv.logfile

    for i in ["pages", "pages/fragments", "objects", "temp"]:
        utils.mkdir(utils.getpath(i))

    try:
        lfile = open(utils.getpath("lock.txt"), 'r+')
    except FileNotFoundError:
        lfile = open(utils.getpath("lock.txt"), 'w+')
    try:
        os.lockf(lfile.fileno(), os.F_TLOCK, 0)
    except BlockingIOError:
        pid = int(lfile.read())
        print(f"Could not start service: Lock file's lock is held by (seemingly) PID {pid}")
        return 2

    utils.clean_temp()

    asyncio.run(do_dbinit(argv))

    lfile.truncate(0)
    if argv.foreground:
        log.openlog(sys.stderr)
        lfile.write(str(os.getpid()))
        lfile.flush()
    else:
        pid = os.fork()
        if pid != 0:
            # parent
            open(argv.config.data.pidfile, 'w').write(str(pid))
            lfile.write(str(pid))
            lfile.close()
            print("Forked to PID", pid)
            return 0
        os.lockf(lfile.fileno(), os.F_LOCK, 0)
        log.openlog(None if argv.syslog else argv.config.data.logfile)

    log.setlevel(argv.loglevel)
    if argv.loglevel == "debug":
        db.db_engine.echo = True

    sock = tornado.netutil.bind_sockets(int(conf.web.port), conf.web.host)

    plugin.init_plugins()

    process.forknvise(
        conf.process.fork_count, conf.process.restart_limit,
        signals=(signal.SIGINT, signal.SIGTERM, signal.SIGUSR1)
    )

    signal.signal(signal.SIGINT, sighand)
    signal.signal(signal.SIGTERM, sighand)
    signal.signal(signal.SIGUSR1, sighand)

    asyncio.run(web.webloop(argv.config.web, sock))

    os.lockf(lfile.fileno(), os.F_ULOCK, 0)
    lfile.close()

    return 0

if __name__ == "__main__":
    raise SystemExit(main(sys.argv))
