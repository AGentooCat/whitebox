from . import utils, config
import time

class TokenEntry:
    def __init__(self, timeout, data=None):
        self.token = utils.random_str()
        self.date = time.time()
        self.timeout = timeout
        self.data = data
        self.pause_time = False

    def is_expired(self, now=None):
        if self.pause_time or self.timeout < 1:
            return False
        return (self.date + self.timeout) <= (now or time.time())

    def finish(self):
        if self.data and (finish := getattr(self.data, "finish", None)):
            finish(self)

    def pause(self):
        self.pause_time = time.time()
    def resume(self):
        if not self.pause_time:
            return
        self.date += (time.time() - self.pause_time)
        self.pause_time = None

tokens = {
    'csrf': {},
    'login': {},
    'folder': {},
}

def clean_tokens():
    now = time.time()
    for space in tokens:
        stale = []
        for (key, token) in tokens[space].items():
            if token.is_expired(now):
                stale.append(key)
        for key in stale:
            tokens[space].pop(key).finish()

def gen_token(space, timeout=None, data=None):
    if not timeout:
        timeout = config.confdat.web.csrf_timeout

    while True:
        if (entry := TokenEntry(timeout)).token not in tokens[space]:
            break
    entry.data = data
    tokens[space][entry.token] = entry
    return entry.token

def get_token(space, token, pop=False, reset_time=False):
    space = tokens[space]
    if token not in space:
        return None

    ctoken = space[token]
    if ctoken.is_expired():
        space.pop(token)
        ctoken.finish()
        return None
    if reset_time:
        space[token].date = time.time()
    elif pop:
        space.pop(token)

    # hack to get bool below return true
    return (ctoken.data,)

def verify_csrf(token, space="csrf", expected=None):
    if not (data := get_token(space, token, pop=True)):
        return False
    if not expected:
        return True
    return data[0] == expected

def time_op(space, token, pause_timer):
    if token not in tokens[space]:
        return False
    if pause_timer:
        tokens[space][token].pause()
    else:
        tokens[space][token].resume()

def remove_token(space, token):
    if token in tokens[space]:
        tokens[space].pop(token).finish()
        return True
    return False

def set_data(space, token, data, update=False):
    if token not in tokens[space]:
        return None
    if not update:
        tokens[space][token].data = data
        return data
    ndat = tokens[space][token].data
    updat = {}
    for k, nv in data.items():
        if k not in ndat:
            ndat[k] = updat[k] = nv
            continue
        ov = ndat[k]
        if nv == -1:
            ov += 1
        elif nv == -2:
            ov -= 1
        else:
            ov = nv
        ndat[k] = updat[k] = ov
    return updat

def look_in_tokendata(space, func):
    for token, value in filter(lambda i: i[1].data, tokens[space].items()):
        if func(token, value):
            return token, value
    return None
