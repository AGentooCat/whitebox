from . import db, pages, web, user, objects, utils, datahands, ipc, plugin
import time

class FileHandler(web.BaseHandler):
    async def get(self, folder, file):
        async with db.db_engine.begin() as conn:
            entry = await db.get_row(db.folder, 'name', folder, conn, True)
            if not entry:
                self.set_status(404)
                return

            fparent, _, fbase = file.rpartition("/")
            fparent = fparent or None
            fentry = await db.get_uuid(db.folder_entry, entry["uuid"], conn, one=True, folder=fparent, name=fbase)
            if not fentry:
                self.set_status(404)
                return
            if not fentry["fuid"]:
                # its a folder
                self.redirect(f"/folder/{folder}/{file}/")
                return

            entry = await db.get_uuid(db.file, fentry["fuid"], conn, True)
            if not entry:
                self.set_status(404)
                return

        self.set_header('Accept-Ranges', 'bytes')
        self.set_header("Cache-Control", "max-age=31536000, public, immutable")

        if ranges := self.request.headers.get("Range"):
            try:
                ranges = utils.get_range_list(ranges[6:], entry['size']) if ranges.startswith('bytes=') else [(None, None)]
            except Exception as _:
                self.set_status(416)
                return
        else:
            self.set_header('Content-Length', str(entry['size']))
            ranges = [(None, None)]


        fd = objects.getobject(entry['hash'], stream=True)
        if len(ranges) == 1:
            start, end = ranges[0]
            if not end:
                end = entry['size']
            if start is not None:
                self.set_header('Content-Length', str(end - start))
                self.set_header('Content-Range', f'bytes {start}-{end - 1}/{entry["size"]}')
                self.set_status(206)

            self.set_header("Content-Type", entry['type'])
            self.set_header('Transfer-Encoding', 'chunked')
            data = datahands.Chunkifier(fd, start=start, end=end)
        else:
            self.set_status(206)
            data = datahands.SplitChunkifier(fd, ranges, entry['type'], entry['size'])
            self.set_header("Content-Type", f'multipart/byteranges; boundary={data.boundary}')

            length = 0
            for i in ranges:
                length += i[1] - i[0]
            self.set_header('Content-Length', str(data.pretend()))
            self.set_header('Transfer-Encoding', 'none')

        await self.send_stream(data)

class FolderThumbHandler(web.BaseHandler):
    async def get(self, name, _):
        async with db.db_engine.begin() as conn:
            entry = await db.get_row(db.folder, 'name', name, conn, True)
            if not entry or not entry['thumb']:
                self.set_status(404)
                return
            thumb = await db.get_uuid(db.file, entry['thumb'], conn, True)
        self.set_header("Cache-Control", "max-age=31536000, public, immutable")
        self.set_header('Content-Type', thumb['type'])
        self.set_header('Content-Length', str(thumb['size']))
        self.write(objects.getobject(thumb['hash']))

async def delete_folder(conn, entry, euser):
    for table in (db.folder, db.folder_entry, db.folder_length):
        await conn.execute(table.delete().where(table.c.uuid == entry["uuid"]))
    for table in (db.account_entries, db.favourites):
        await conn.execute(table.delete().where(table.c.euid == entry["uuid"]))
    await conn.execute(db.history.delete().where(
        table.c.uuid == entry["uuid"],
        table.c.table == "folder"
    ))
    ipc.request_ipc("remove_folder", name=entry["name"])

    newfoldlen = ipc.request_ipc("update_other_user", uuid=euser["uuid"], data={"foldlen": -2}) or {"foldlen": euser["foldlen"] - 1}
    await conn.execute(db.account.update().where(
        db.account.c.uuid == euser['uuid']
    ), newfoldlen)

    async for i in await db.get_rows(db.favourites, "euid", entry["uuid"], conn):
        i = await db.get_uuid(db.account, i["uuid"], conn, True)
        newfavslen = ipc.request_ipc("update_other_user", uuid=i["uuid"], data={"favslen": -2}) or {"favslen": i["favslen"] - 1}
        await conn.execute(db.account.update().where(
            db.account.c.uuid == i['uuid']
        ), newfavslen)

class FolderHandler(web.BaseHandler):
    async def _handlefav(self, conn, entry, fav):
        favtable = db.favourites

        if self.current_user:
            data = {
                'uuid': self.current_user['uuid'],
                'euid': entry['uuid']
            }
            notfavd = not bool((await conn.execute(favtable.select().where(
                favtable.c.uuid == data['uuid'],
                favtable.c.euid == data['euid']
            ))).fetchone())
        else:
            notfavd = None

        if not (fav in ['yes', 'no', '1', '0'] and self.current_user):
            return notfavd

        upval = None
        if notfavd and fav in ['yes', '1']:
            data = await conn.execute(favtable.insert().values(data))
            notfavd = False
            upval = (-1, entry["favourites"] + 1)
        elif not notfavd and fav in ['no', '0']:
            data = await conn.execute(favtable.delete().where(
                favtable.c.uuid == data['uuid'],
                favtable.c.euid == data['euid']
            ))
            notfavd = True
            upval = (-2, entry["favourites"] - 1)

        if upval is not None:
            await self.update_user({"favslen": upval[0]}, conn)
            await conn.execute(db.folder.update().where(
                db.folder.c.uuid == entry['uuid']
            ), {'favourites': upval[1]})
            ipc.request_ipc("folder_mod", name=entry["name"], rtype="favourite", up=(upval[1] > entry["favourites"]))
            entry["favourites"] = upval[1]

        return notfavd

    async def get(self, name, inner=None):
        if inner:
            inner = utils.norm(inner)
        inner = inner or None

        async with db.db_engine.begin() as conn:
            entry = await db.get_row(db.folder, 'name', name, conn, True)
            if not entry:
                return self.send404()
            euser = await db.get_uuid(db.account, entry['creator'], conn, True)
            thumb = await db.get_uuid(db.file, entry["thumb"], conn, True) if entry["thumb"] else None

            if self.isadmin() and self.get_argument("delete", allowed=("yes",)):
                await delete_folder(conn, entry, euser)
                return self.redirect("/")

            flen = (await db.get_uuid(db.folder_length, entry["uuid"], conn, name=inner, one=True))["len"]
            fentries = []
            async for fent in await db.get_rows(db.folder_entry, "uuid", entry['uuid'], conn, offpair=self.pagepair(), folder=inner):
                fent = dict(fent)
                if fent["fuid"]:
                    fent |= await db.get_uuid(db.file, fent["fuid"], conn, True) | {
                        "name": fent["name"],
                        "folder": fent["folder"]
                    }
                fentries.append(fent)

            ipc.request_ipc("folder_mod", name=name, rtype="view", up=True)
            await conn.execute(db.folder.update().where(db.folder.c.name == name), {'views': entry['views'] + 1})

            notfavd = await self._handlefav(conn, entry, self.get_argument('fav', 'none'))

        fdrows = []
        for i in fentries:
            pfold = ("{}/".format(i["folder"]) if i["folder"] else "") + i["name"]
            _, _, fbase = i["name"].rpartition("/")
            if i["fuid"]:
                fdrows.append([(entry["name"], pfold, fbase), i['size'], i['type']])
            else:
                fdrows.append([(entry["name"], pfold + "/", fbase + "/", 'style="color: lightgreen"'), -1, "[folder]"])

        favlink = ''
        if notfavd is not None:
            text = 'Add to' if notfavd else 'Remove from'
            favlink = f' (<a href="?fav={"yes" if notfavd else "no"}">{text} your favourites</a>)'

        thumblink = ''
        if thumb:
            ext = thumb["type"].rpartition("/")[2]
            thumblink = f'<img src="/fthumb/{utils.url_escape(entry["name"])}.{ext}" width="256" alt="thumbnail for folder {utils.url_escape(entry["name"])!r}"/><br>'

        currentlyat = ""
        if inner:
            fdrows.insert(0, [
                (entry["name"], inner.rpartition("/")[0], "..", 'style="color: lightgreen"'),
                 -1, "[parent folder]"
            ])

            done = "/folder/" + entry["name"]
            currentlyat = f'<br><div style="text-align: center">Currently at subfolder: <code><a href="{utils.url_escape(done)}">[root]</a>'
            isplit = inner.split("/")
            for (n, part) in enumerate(isplit):
                done += "/" + part
                part = utils.xhtml_escape(part)
                if n < len(isplit) - 1:
                    part = f'<a href="{utils.url_escape(done)}">{part}</a>'
                currentlyat += f'/{part}'
            currentlyat += "</code></div>"

        controls = []
        if self.isadmin():
            controls.append('<a href="?delete=yes">Delete this folder</a>')
        if (self.current_user or {}).get("uuid") == euser["uuid"]:
            appendhere = f'<a href="/append/{utils.url_escape(entry["name"])}">Append to this folder</a>'
            if inner:
                appendhere += f' | <a href="/append/{utils.url_escape(entry["name"])}?inner={utils.url_escape(inner)}">Append to this subfolder</a>'
            controls.append("")
            controls.append(appendhere)

        self.make_page(f'Folder "{utils.xhtml_escape(entry["name"])}"', pages.get_fragment('folder-page', keys={
            'name': entry["name"],
            'username': user.color_user(euser, withlink=True),
            "uploadtime": time.ctime(entry["date"]),
            'uuid': entry["uuid"],
            'entries': pages.build_table_frag('file_entries', fdrows, klasse="list"),
            'views': entry['views'] + 1,
            'downloads': entry['downloads'],
            'size': utils.get_human_size(entry['size']),
            'favlink': favlink,
            'thumblink': thumblink,
            'page-links:pagelinks': datahands.gen_pagelinks(fdrows, self.page, self.page_size, flen),
            "currentlyat": currentlyat,
            "description": entry["description"] or "",
            "controls": '<br>'.join(controls),
            "favourites": entry["favourites"],
        }, indent=0))

class DownloadHandler(web.BaseHandler):
    async def get(self, folder, fmt):
        cmp = bool(self.get_argument("compress"))
        ctype = None
        if (pluhand := plugin.check_arg_in_category("ExportFormat", fmt)):
            stream, ctype, cmp = pluhand.run(fmt)
        elif fmt == "zip":
            stream = datahands.WrappedZip(cmp)
        elif fmt == "tar":
            stream = datahands.WrappedTar(cmp)
        elif fmt == "tgz":
            cmp = True
            stream = datahands.WrappedTar(True)
        else:
            return self.send404()

        async with db.db_engine.begin() as conn:
            entry = await db.get_row(db.folder, 'name', folder, conn, True)
            if not entry:
                self.set_status(404)
                return
            fentries = await db.get_uuid(db.folder_entry, entry['uuid'], conn)
            files = {ent["uuid"]: ent for ent in [
                await db.get_uuid(db.file, i["fuid"], conn, True)
                for i in filter(lambda i: i["fuid"], fentries)
            ]}
            thumb = await db.get_uuid(db.file, entry["thumb"], conn, True) if entry["thumb"] else None
            await conn.execute(db.folder.update().where(db.folder.c.uuid == entry["uuid"]), {'downloads': entry['downloads'] + 1})
        ipc.request_ipc("folder_mod", name=entry["name"], rtype="download", up=True)

        if thumb:
            stream.add(folder + "." + thumb["type"].rpartition("/")[2], thumb)
        for ent in fentries:
            path = ent["name"]
            if ent["folder"]:
                path = ent["folder"] + "/" + path
            path = folder + "/" + path

            stream.add(path, files.get(ent["fuid"]))

        self.set_status(200)
        self.set_header("Content-Type", ctype or ("application/" + {
            "zip": "zip",
            "tar": "x-tar",
            "tgz": "gzip",
        }.get(fmt)))
        if not cmp:
            self.set_header("Content-Length", stream.len())
        self.set_header("Last-Modified", time.strftime("%a, %d %b %Y %H:%M:%S %Z", time.gmtime(entry["date"])))
        self.set_header("Cache-Control", "max-age=31536000, public, immutable")

        self.set_header('Transfer-Encoding', 'chunked')
        data = datahands.Chunkifier(stream)

        await self.send_stream(data)
