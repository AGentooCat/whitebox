from . import db, pages, web, utils, datahands
import re

class SearchHandler(web.BaseHandler):
    async def _search(self, term, space):
        _ = re.compile(term)
        rows = []

        if space == 'folder':
            table = (db.folder, db.folder.c.name.regexp_match(term))
        elif space == 'category':
            table = (db.folder, db.folder.c.category == term)
        elif space == 'user':
            table = (db.account, db.account.c.username.regexp_match(term))
        offset, length = self.pagepair(True)
        async with db.db_engine.begin() as conn:
            entries = [dict(i) for i in (await conn.execute(table[0].select().where(table[1]).offset(offset).limit(length))).mappings().fetchall()]

            if space in ('folder', 'category'):
                for i in entries:
                    i = await db.reformat_folder(i, conn)
                    rows.append([i["thumb"], i['name'], i['date'], i['category'], i['creator'], (i["views"], i["favourites"])])
                table = 'folder_results'
            elif space == 'user':
                for i in entries:
                    count = len(await db.get_uuid(db.account_entries, i['uuid'], conn))
                    rows.append([i, str(count), i['date']])
                table = 'user_entries'

        plinks = datahands.gen_pagelinks(rows, self.page, self.page_size, -1, extra=f'q={utils.url_escape(term)}&space={space}')
        if len(rows) == self.page_size + 1:
            rows = rows[:-1]
        tfrag = pages.build_table_frag(table, rows)

        return pages.get_fragment("search-result", keys={
            'results': tfrag,
            'page-links:pagelinks': plinks,
        })

    async def get(self):
        term = self.get_argument('q', '')
        space = self.get_argument('space', 'folder')

        msgtext = ''
        results = ''
        if not term:
            msgtext = 'Please submit a term to search above.'

        if space not in ['user', 'folder', 'category']:
            msgtext = 'Please search in a valid area (user, folder).'

        if not msgtext:
            try:
                results = "<hr>\n" + await self._search(term, space)
            except re.error:
                msgtext = "Invalid search term."

        self.make_page('Search', pages.get_fragment("search-page", keys={
            "searchresults": results,
            "search-form:term": term,
            "search-form:" + (space[:3] + "select"): "selected",
        }), msgtext=msgtext)
