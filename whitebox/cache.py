from . import config

cache = {}
def init_cache(folders, users, files, fsize, last_folders, last_users):
    global cache
    cache |= {
        "folders": folders,
        "users": users,
        "files": files,
        "total_size": fsize,
        "last_folders": last_folders,
        "last_users": last_users
    }

def fetch_cache(field, off, rlen):
    if field == "meta":
        return {
            "folders": cache["folders"],
            "users": cache["users"],
            "files": cache["files"],
            "total_size": cache["total_size"]
        }
    sect = cache["last_" + field + "s"]
    rlen = None if rlen is None else off + rlen
    return sect[off:rlen], len(sect)

def _get_entry(field, name, infield="name"):
    for entry in cache["last_" + field + "s"]:
        if entry[infield] == name:
            return entry
    return None

def add_user(user):
    cache["users"] += 1
    user |= {"password": None}
    cache["last_users"].insert(0, user)
    cache["last_users"] = cache["last_users"][:config.confdat.cache.account_limit]

def user_mod(user, mtype, val):
    if not (user := _get_entry("user", user, infield="uuid")):
        return
    def getval(inval):
        if val == -1:
            return inval + 1
        elif val == -2:
            return inval - 1
        return val

    if mtype == "folder":
        user["foldlen"] += getval(user["foldlen"])
    elif mtype == "favourite":
        user["favslen"] += getval(user["favslen"])
    elif mtype == "type":
        user["type"] = val

def add_folder(folder):
    folder, tdata, files, userdata = folder
    folder["views"] = folder["downloads"] = folder["favourites"] = 0
    folder["creator"] = userdata | {"password": None}
    folder["thumb"] = tdata
    if tdata:
        folder["thumb"]["name"] = folder["name"]
    cache["folders"] += 1
    cache["last_folders"].insert(0, folder)
    cache["last_folders"] = cache["last_folders"][:config.confdat.cache.folder_limit]

    fsize = 0
    for f in files:
        fsize += f["size"]
    cache["files"] += len(files)
    cache["total_size"] += fsize

def append_folder(folder):
    fentry, files = folder
    cache["files"] += len(files)
    cache["total_size"] += sum(i["size"] for i in files)

    if not (folder := _get_entry("folder", fentry["name"])):
        return
    folder["len"] = fentry["len"]
    folder["size"] = fentry["size"]

def folder_mod(folder, mtype, inc):
    if not (folder := _get_entry("folder", folder)):
        return
    inc = 1 if inc else -1
    if mtype == "view":
        folder["views"] += inc
    elif mtype == "download":
        folder["downloads"] += inc
    elif mtype == "favourite":
        folder["favourites"] += inc

def remove_folder(folder):
    cache["folders"] -= 1
    if (folder := _get_entry("folder", folder)):
        cache["last_folders"].remove(folder)
        return
