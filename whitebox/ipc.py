import select, logging, threading, time
from . import utils, process, tokens, config, cache, datahands, errors
log = logging.getLogger(__name__)

reqlock = threading.Lock()
def request_ipc(comtype, **args):
    data = {"type": comtype} | dict(args)
    reqlock.acquire()
    utils.sendobj(utils.tosuper, data)
    ret = utils.recvobj(utils.tosuper)
    reqlock.release()
    return ret

##########################################################
#### No children processes allowed beyond this point. ####
##########################################################

# dict(user_token, dict(folder_token, Folder))
current_uploads = {}
# shorthand for folder names
current_upload_names = set()

class Folder:
    name = None
    desc = None
    thumb = None
    category = None
    msg = None

    size = None

    # the difference: files[] contains files that the children are interested in
    # filelist{} is for ID keeping
    existing = None
    files = None
    filelist = None
    curholds = None

    infinal = False

    def _init(user, name, desc, thumb, category, uuid=None, existing=None, inner=None):
        if name in current_upload_names:
            raise errors.InvalidFolderError("Folder name already exists.")
        if not utils.check_str(name):
            raise errors.InvalidFolderError('Folder name invalid')
        if not uuid and not utils.check_str(category):
            raise errors.InvalidFolderError('Folder category invalid')

        self = Folder()
        self.uuid = uuid or utils.get_uuid()
        self.user = user[0]
        self.userdata = user[1]
        if "password" in self.userdata:
            self.userdata.pop("password")
        self.name = name
        self.desc = desc
        self.thumb = thumb
        self.category = category
        self.existing = existing or dict()
        self.inner = inner

        self.size = 0
        self.files = 0
        self.filelist = {}
        self.curholds = set()

        current_upload_names.add(name)
        user = user[0]
        current_uploads.setdefault(user, {})[name] = self

        return self

    def set_msg(self, msg):
        self.msg = msg
    def pop_msg(self):
        msg = self.msg
        self.msg = None
        return msg

    def _select_name(self, name):
        name = utils.norm(name)
        files = {i.name for i in filter(lambda i: not i.pending, self.filelist.values())}
        files.update(self.existing.keys())

        if self.inner:
            name = self.inner + "/" + name
        if name not in files:
            return name

        folder, _, name = name.rpartition("/")
        name, _, ext = name.rpartition('.')
        if not _:
            name = ext
            ext = ''

        if folder:
            folder += '/'
        if ext:
            ext = '.' + ext

        n = 0
        while True:
            nname = f'{folder}{name}_{n}{ext}'
            if nname not in files:
                return nname
            n += 1

    def _select_nind(self):
        self.files += 1
        return self.files

    def _check_name(self, name):
        return bool(name.rpartition("/")[2])

    def add_file(self, fent):
        if self.curholds is None:
            return False
        if not self._check_name(fent.name):
            raise errors.InvalidFilenameError(fent.name, "Invalid filename")
        fent._find = nind = self._select_nind()
        fent.name = self._select_name(fent.name)
        self.size += fent.size
        self.filelist[nind] = fent
        return nind

    def add_files(self, fents):
        ninds = []
        for fent in fents:
            if not self._check_name(fent.name):
                raise errors.InvalidFilenameError(fent.name, "Invalid filename")
        for fent in fents:
            if (i := self.add_file(fent)) is False:
                return False
            ninds.append(i)
        return tuple(ninds)

    def new_file(self, init_msg, parent=None):
        if self.curholds is None:
            return False
        if parent is not None and not self._chk_find(parent):
            return False
        nind = self._select_nind()
        self.filelist[nind] = datahands.FileEntry(dofile=False)
        self.filelist[nind].pending = init_msg
        self.filelist[nind]._find = nind
        self.curholds.add(nind)
        return nind

    def _chk_find(self, find):
        return self.curholds is not None and find in self.filelist

    def _with_find(self, find, field, data, deccur=False, close=False):
        if not self._chk_find(find):
            return False
        if field is None:
            self.filelist[find] = data
            self.filelist[find]._find = find
        else:
            setattr(self.filelist[find], field, data)
        if deccur:
            self.curholds.discard(find)
            self.size += self.filelist[find].size
        if close:
            self.filelist[find].close()
        return True

    def get_find(self, find):
        if not self._chk_find(find):
            return False
        return self.filelist[find]

    def pop_find(self, find):
        if not self._chk_find(find):
            return
        if self.filelist.pop(find).pending:
            self.curholds.discard(find)

    def size_file(self, find, size):
        return self._with_find(find, "size", size)

    def set_file(self, find, fent):
        fent.name = self._select_name(fent.name)
        return self._with_find(find, None, fent, True)

    def skip_file(self, find, msg):
        return self._with_find(find, "skipthis", msg, True)

    def msg_file(self, find, msg):
        return self._with_find(find, "pending", msg)

    def fail_file(self, find, msg):
        return self._with_find(find, "failmsg", msg, True)

    def can_commit(self):
        if self.infinal:
            return "Folder already being saved..."
        if len(self.filelist) == 0:
            return "No files in folder; you need to upload some files."
        if self.curholds:
            return "A file is still pending."

        failed = 0
        skipped = 0
        for fent in self.filelist.values():
            if fent.failmsg:
                failed += 1
            elif fent.skipthis:
                skipped += 1
            elif fent.pending:
                return "A file is still pending."
        if (failed + skipped) == len(self.filelist):
            return "All files have failed or got skipped; you need to upload some files."
        return None

    def commit(self):
        if (msg := self.can_commit()):
            return msg
        self.infinal = True
        return self

    def finish(self, _):
        if self.thumb:
            self.thumb["data"].close()
        for file in self.filelist.values():
            file.close()
        self.filelist.clear()
        self.existing.clear()
        self.curholds = None
        current_upload_names.discard(self.name)
        current_uploads[self.user].pop(self.name)

def _add_follist(child, folder, item):
    fset = child["folders"].setdefault(folder, set())
    if fset is not None:
        fset.add(item)
def _rem_follist(child, folder, item):
    if isinstance(child["folders"].get(folder), set):
        child["folders"][folder].discard(item)

onefoldliners = {
    "valid_find": lambda com, folder: folder._chk_find(com["find"]),
    "set_fmsg": lambda com, folder: folder.set_msg(com["msg"]),
    "add_file": lambda com, folder: folder.add_file(com["file"]),
    "add_files": lambda com, folder: folder.add_files(com["files"]),
    "pop_find": lambda com, folder: folder.pop_find(com["find"]),
    "get_find": lambda com, folder: folder.get_find(com["find"]),
    "can_commit": lambda _, folder: folder.can_commit(),
    "commit": lambda _, folder: folder.commit(),
    "pause_folder": lambda com, folder: tokens.time_op("folder", com["token"], com["do_pause"]),
}

oneliners = {
    "check_login": lambda com: tokens.get_token('login', com["token"], reset_time=True),
    "logout": lambda com: tokens.verify_csrf(com["token"], "login"),
    "update_user": lambda com: tokens.set_data("login", com["token"], com["data"], update=True),
    "verify_csrf": lambda com: tokens.verify_csrf(com["token"], "csrf", expected=com["url"]),
    "get_csrf": lambda com: tokens.gen_token("csrf", timeout=config.confdat.web.csrf_timeout, data=com["next_url"]),

    "folder_mod": lambda com: cache.folder_mod(com["name"], com["rtype"], com["up"]),
    "user_mod": lambda com: cache.user_mod(com["uuid"], com["rtype"], com["val"]),
    "fetch_cache": lambda com: cache.fetch_cache(com["rtype"], com["off"], com["len"]),
    "remove_folder": lambda com: cache.remove_folder(com["name"]),
}
def handle_ipc(child, com, _override_fuser=False):
    if (func := oneliners.get(com["type"])):
        return func(com)
    elif com["type"] in ("login_user", "register"):
        if com["type"] == "register":
            cache.add_user(com["data"])
        return tokens.gen_token("login", timeout=config.confdat.web.login_timeout, data=com["data"]) if com["data"]["activated"] else None
    elif com["type"] == "update_other_user":
        usertoken = tokens.look_in_tokendata("login", lambda _, value: value.data["uuid"] == com["uuid"])
        return tokens.set_data("login", usertoken[0], com["data"], update=True) if usertoken else None
    elif com["type"] == "new_folder":
        if com.get("exist_list"):
            folder = Folder._init(
                (com["user_token"], com["userdata"]),
                com["name"], None, None, None,
                uuid=com.get("exist_uuid"),
                existing=com.get("exist_list"),
                inner=com.get("inner"),
            )
        else:
            folder = Folder._init((com["user_token"], com["userdata"]), com["name"], com["desc"], com["thumb"], com["category"])
        timeout = -1 if com.get("process_lived") else config.confdat.web.upload_status_timeout
        return tokens.gen_token("folder", timeout=timeout, data=folder)

    # else: assuming folder stuff
    if com["type"] == "get_folder" and com.get("all"):
        return current_uploads
    folder = tokens.get_token("folder", com["token"], reset_time=True)
    if not folder or (not _override_fuser and folder[0].user != com["user_token"]):
        return None
    folder = folder[0]
    if com.get("reset_pause"):
        tokens.time_op("folder", com["token"], False)

    if (func := onefoldliners.get(com["type"])):
        return func(com, folder)
    elif com["type"] == "get_folder":
        if com.get("nometa"):
            return folder.name
        data = {
            "token": com["token"],
            "name": folder.name,
            "desc": folder.desc,
            "size": folder.size,
            "flen": len(folder.filelist),
            "thumb": folder.thumb,
            "msg": folder.pop_msg(),
            "existing": bool(folder.existing),
            "inner": folder.inner,
            "infinal": folder.infinal,
        }
        if com.get("floff"):
            start, length = com["floff"]
            data["flrows"] = list(folder.filelist.values())[start:start+length]
        return data
    elif com["type"] == "new_file":
        find = folder.new_file(com["init_msg"], parent=com.get("parent"))
        _add_follist(child, com["token"], find)
        return find
    elif com["type"].endswith("_file"):
        whatfile = com["type"].partition("_")[0]
        whatfield = {"skip": "msg", "fail": "msg", "set": "file"}.get(whatfile, whatfile)
        if getattr(folder, com["type"])(com["find"], com[whatfield]) and whatfile in ("skip", "fail", "set"):
            _rem_follist(child, com["token"], com["find"])
    elif com["type"] == "close":
        if com["token"] in child["folders"]:
            child["folders"].pop(com["token"])
        tokens.remove_token("folder", com["token"])
        if com["foldat"]:
            if com.get("existing"):
                cache.append_folder(com["foldat"])
            else:
                cache.add_folder(com["foldat"])
    return None

def ipc_loop():
    until_clean = 300
    while process.children:
        try:
            maps = {i["tochild"].fileno(): i for i in process.children.values()}
        except RuntimeError: # dict changed size while iterating - main thread wait()ed
            continue
        phand = select.poll()
        for fd in maps:
            phand.register(fd, select.POLLIN | select.POLLHUP)

        bepoll = time.time()
        evlist = phand.poll(2000)

        until_clean -= (time.time() - bepoll)
        if until_clean < 0:
            tokens.clean_tokens()
            until_clean = 300

        for (fd, stat) in evlist:
            if not (child := maps.get(fd)):
                continue
            if stat & select.POLLNVAL:
                # socket closed - we closed it; next iteration shouldn't have it
                continue

            data = None
            if stat & select.POLLIN:
                try:
                    data = utils.recvobj(child["tochild"])
                except OSError:
                    # most likely invalid fd = conn shut
                    data = None
                if data:
                    try:
                        ret = handle_ipc(child, data)
                    except Exception as e:
                        log.critical("Got exception; bouncing back", exc_info=True)
                        ret = e
                    log.debug("%r => %r", data, ret)
                    try:
                        utils.sendobj(child["tochild"], ret)
                    except Exception:
                        pass

            if (stat & select.POLLHUP) and not data:
                continue
