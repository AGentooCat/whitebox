import multiprocessing, os, signal, socket, threading, logging, sys
from . import utils, ipc
log = logging.getLogger(__name__)

children = {}
def sigtochild(signum, _):
    for child in children:
        os.kill(child, signum)
        if signum != signal.SIGUSR1:
            children[child]["currsig"] = signum

def forknvise(maxproc=None, maxrest=None, signals=None):
    maxproc = maxproc or (multiprocessing.cpu_count() if multiprocessing else 1)
    signals = signals or set()

    psocks = {}
    def forkout():
        spline, chline = socket.socketpair()
        psocks[spline.fileno()] = spline

        pid = os.fork()
        if pid == 0:
            for sock in psocks.values():
                sock.close()
            utils.tosuper = chline
            return True

        chline.close()
        children[pid] = {
            "pid": pid,
            "currsig": None,
            "tochild": spline,
            "folders": {},
        }
        return False

    def popclose(pid):
        child = children.pop(pid)
        psocks.pop(child["tochild"].fileno()).close()
        for folder, finds in list(child["folders"].items()): # copy because ipc->close mods this dict
            if not finds:
                # the whole folder
                ipc.handle_ipc(child, {
                    "type": "close",
                    "token": folder,
                }, _override_fuser=True)
                continue
            for find in finds:
                ipc.handle_ipc(child, {
                    "type": "pop_find",
                    "token": folder,
                    "find": find,
                }, _override_fuser=True)

    for n in range(maxproc):
        if forkout():
            return True

    for signum in signals:
        signal.signal(signum, sigtochild)

    ipc_looper = threading.Thread(target=ipc.ipc_loop)
    ipc_looper.start()

    remstart = maxrest or 100
    while children:
        pid, pstat = os.wait()
        if pid not in children:
            continue
        dorest = "restarting" if remstart > 0 else "over restart limit - not restarting"
        if os.WIFSIGNALED(pstat):
            log.warning("Child %d was signalled %d; %s", pid, os.WTERMSIG(pstat), dorest)
        elif os.WEXITSTATUS(pstat) == 0 and children[pid]["currsig"] is not None:
            # we gave this signal
            log.info("Child %d received our signal %d", pid, children[pid]["currsig"])
            popclose(pid)
            continue
        else:
            log.warning("Child %d exited with %d; %s", pid, os.WEXITSTATUS(pstat), dorest)
        popclose(pid)
        if remstart == 0:
            continue
        if forkout():
            return True
        remstart -= 1
        if remstart == 0:
            log.warning("Above restart limit of %d; no more restarts")

    ipc_looper.join()
    log.info("All children exited; closing WhiteBox")
    sys.exit(0)
