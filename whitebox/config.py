import tomllib, re
from . import errors
from urllib.parse import urlparse

########################## CONFIGURATION DEFAULTS #############################
class Config:                           # main config struct
    class data:                         # data directories
        maindir = "/var/lib/whitebox"    # main directory; relatives point here
        max_upload_size = 52428800        # max upload size for files, archive imports, etc
        logfile = "/var/log/whitebox.log" # logfile to use
        pidfile = "/run/whitebox.pid"    # pidfile to use too
        user = "whitebox"               # user to switch to (datadir must be
        group = "whitebox"              #   + group          owned by this)

    class database:                     # database info
        type = "sqlite"                 # type: ["postgres", "sqlite", "mysql"]
        host = "127.0.0.1:5432"         # host:post
        user = "whitebox"               # user in database
        password = "whitebox"           # password in database
        database = "whitebox"           # database name in... database

    class web:
        host = "localhost"              # standard host
        port = 8080                     #    and port
        csrf_timeout = 60               # how long do the csrf tokens live for
        upload_status_timeout = 600     # how long do the upload status pages live for
        login_timeout = 600             # i wonder what this is
        chunk_size = 1048576            # chunk size for tornado's buffers
        categories = [                  # content categories for folders
            'uncategorized',
            'texts',
            'videos',
            'audio',
            'pictures',
            'software',
        ]
        page_size = 30                  # page size for entries
        disable_registration = False    # disable the /register endpoint
        disable_upload = False          # disable the /upload endpoint
        # DO NOT ENABLE THIS EXCEPT FOR DEVELOPMENT
        # THIS BYPASSES AUTHENTICATION COMPLETELY
        bypass_logins = False
        require_activation = True       # a new account needs to be activated
        enable_request_logs = False     # enable request logging
                                        #   (bad alternative: -Ldebug)

    class upload:
        user_agent = "Mozilla/5.0 (compatible; pycurl + WhiteBox)"
            # the HTTP User-Agent to use when importing from URLs
            # doesn't affect plugins (unless one decides to use this as well)
        thumb_max_size = 1048576        # maximum size of a thumbnail file
        thumb_max_width = 1024          #   and width
        thumb_max_height = 1024         #   and height

    class cache:
        folder_limit = 50               # how many folder entries to keep (max)
        account_limit = 50              # same for accounts

    class api:
        api_limit = 30                  # limit an API key to X requests
        api_limit_time = 30             #    every Y seconds
        block_time = 300                #    and block a bad one for Z seconds

    class process:
        fork_count = 8                  # how many processes to fork into
        restart_limit = 100             # how many times to restart dead procs
        # how many threads to run at once
        # this limits file servings, folder downloads and utils.passcode
        thread_limit = 100

    class proxy:                        # proxy list
        # [regex for host, proxy for it]
        # [regex for host, proxy for it, extra opts]
        # [default proxy, none if not given]
        list = [
            [".*\\.i2p", "http://127.0.0.1:4444"],
            [".*\\.onion", "socks5h://127.0.0.1:9050", {}],
            ["http://127.0.0.1:8888"]
        ]
###############################################################################



class Proxy:
    matcher = None
    proxy = None
    extra = None

    def __init__(self, matcher, proxy, extra=None):
        if matcher:
            self.matcher = re.compile(matcher)
        self.proxy = proxy
        self.extra = extra

    def matches(self, host):
        if not self.matcher:
            return True
        return self.matcher.match(host) is not None

def recurse_parse(data, config):
    # NOTE: this seems trustable because
    #       1. first invocation has curkey = '' => eval('config')
    #       2. getattr below checks if Config has the value before
    #          recursive-calling us
    for key in data:
        try:
            wanted = getattr(config, key)
            if isinstance(wanted, (Config, type)):
                attr = recurse_parse(data[key], wanted)
            else:
                attr = data[key]
            if not isinstance(wanted, type(attr)):
                raise errors.WrongKeyTypeError(type(wanted).__name__, key, type(attr).__name__)
            setattr(config, key, attr)
        except AttributeError:
            raise errors.MissingKeyError(key)
    return config

confdat = None

def parse_config(filename):
    data = tomllib.load(open(filename, 'rb')) if filename else {}
    config = recurse_parse(data, Config())

    if config.web.bypass_logins:
        print("! Authentication disabled. Logins will be done passwordless. Enable only for development.")

    proxies = []
    for proxy in config.proxy.list:
        matcher, url, extra = None, proxy[0], None
        if len(proxy) > 1:
            matcher = url
            url = proxy[1]
            extra = proxy[2] if len(proxy) == 3 else None
        if len(proxy) > 3:
            raise errors.OverflowingProxyError(proxy)
        url = urlparse(url)
        proxies.append(Proxy(matcher, url, extra=extra))
    config.proxy.list = proxies

    host = config.database.host
    host, _, config.database.port = host.rpartition(':')
    if _ == "":
        host = config.database.port
        config.database.port = None
    config.database.host = host

    global confdat
    confdat = config
    return config
