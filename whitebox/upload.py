from . import db, utils, pages, web, config, objects, datahands, errors, thread, ipc, url
import tornado.web, time, io, logging, queue
from PIL import Image, UnidentifiedImageError
log = logging.getLogger(__name__)

def get_thumb_fent(data, name):
    if data is None:
        return None
    if len(data) > config.confdat.upload.thumb_max_size:
        raise errors.InvalidThumbnailError('Thumbnail too large')

    try:
        thumb = Image.open(io.BytesIO(data), formats=("JPEG", "PNG"))
    except UnidentifiedImageError:
        raise errors.InvalidThumbnailError('Wrong thumbnail format')
    except Image.DecompressionBombError:
        raise errors.InvalidThumbnailError('Thumbnail too large')
    except Exception:
        raise errors.InvalidThumbnailError('Thumbnail might be broken')

    w, h = thumb.width, thumb.height
    thumb.close()
    if w > config.confdat.upload.thumb_max_width or h > config.confdat.upload.thumb_max_height:
        raise errors.InvalidThumbnailError('Thumbnail too large')

    fent = datahands.FileEntry(f'<thumb for {name!r}>')
    fent.write(data)
    fent.update_mime()
    fent.finish()

    return {
        "data": fent,
        "width": w,
        "height": h,
        "type": fent.mime.rpartition('/')[2],
    }

def _deconst2foldent(suid, fuid, name, fentries, subfolders):
    follist, _, name = name.rpartition("/")
    d2t = lambda d: tuple(sorted(d.items(), key=lambda i: i[0]))
    fentries.add(d2t({ # sets can't hash dicts so we use a tuple
        'uuid': suid,
        'fuid': fuid,
        "folder": follist or None,
        'name': name,
    }))

    if not follist:
        return
    fparent = None
    for fpart in follist.split("/"):
        subfolders.add(d2t({
            "uuid": suid,
            "fuid": None,
            "folder": fparent,
            "name": fpart
        }))
        fparent = f"{fparent}/{fpart}" if fparent else fpart

async def putobjects(self, fentry, conn, now):
    files = []
    fentries, subfolders = set(), set()
    for file in self.filelist.values():
        if file.failmsg or file.skipthis:
            continue

        file.update_mime()
        if not file._finished:
            file.finish()

        status = await objects.putobject(conn, file, now, files)
        file.uuid = status[2]["uuid"]
        if not status[1]:
            files.append(status[2])

        fentry['size'] += file.size
        fentry["len"] += 1

        _deconst2foldent(self.uuid, file.uuid, file.name, fentries, subfolders)
    return files, fentries, subfolders

async def recommit_folder(self, conn):
    now = int(time.time())
    fentry = await db.get_uuid(db.folder, self.uuid, conn, True)

    t2d = lambda t: [dict(i) for i in t]
    files, fentries, subfolders = await putobjects(self, fentry, conn, now)

    flens, nflens = {}, {}
    for ename in self.existing:
        efolder, _, name = ename.rpartition("/")
        efolder = efolder or None
        flens[efolder] = flens.get(efolder, 0) + 1
    for foldent in fentries.union(subfolders):
        foldent = dict(foldent)["folder"]
        if foldent in flens:
            flens[foldent] += + 1
        else:
            nflens[foldent] = nflens.get(foldent, 0) + 1
    flens = [{"uuid": self.uuid, "name": f, "len": n} for f, n in flens.items()]
    nflens = [{"uuid": self.uuid, "name": f, "len": n} for f, n in nflens.items()]

    getfname = lambda i: (i["folder"] + "/" if i["folder"] else "") + i["name"]
    for fent in filter(lambda i: i["fuid"], self.existing.values()):
        _deconst2foldent(fentry["uuid"], fent["fuid"], getfname(fent), fentries, subfolders)
    sorter = lambda lst: sorted(t2d(lst), key=lambda i: getfname(i))
    fentries = sorter(subfolders) + sorter(fentries)

    await conn.execute(db.folder.update().where(db.folder.c.uuid == self.uuid), {"size": fentry["size"], "len": fentry["len"]})
    await conn.execute(db.folder_entry.delete().where(db.folder_entry.c.uuid == fentry["uuid"]))
    await conn.execute(db.folder_entry.insert(), fentries)
    if nflens:
        await conn.execute(db.folder_length.insert(), nflens)
    for flen in flens:
        await conn.execute(db.folder_length.update().where(
            db.folder_length.c.uuid == flen["uuid"],
            db.folder_length.c.name == flen["name"]
        ), {"len": flen["len"]})
    if len(files) > 0:
        await conn.execute(db.file.insert(), files)
        await conn.execute(db.history.insert(), {
            "uuid": fentry["uuid"],
            "date": fentry["date"],
            "table": "folder",
            "field": "new_files",
            "data": ";".join(i["uuid"] for i in files),
        })
    await conn.commit()

    return fentry, files

async def _commit_folder(self, conn):
    now = int(time.time())
    fentry = {
        'uuid': self.uuid,
        'creator': self.userdata["uuid"],
        'category': self.category,
        "description": self.desc,
        'date': now,
        'name': self.name,
        'size': 0,
        "len": 0,
    }
    files, tdata = [], None
    if self.thumb:
        status = await objects.putobject(conn, self.thumb["data"], now, files)
        fentry['thumb'] = status[2]["uuid"]
        if not status[1]:
            files.append(status[2])
        tdata = {
            "uuid": fentry["uuid"],
            "fuid": fentry["thumb"],
            "width": self.thumb["width"],
            "height": self.thumb["height"],
            "type": self.thumb["type"],
        }
    ants = {
        'uuid': self.userdata['uuid'],
        'type': 0,
        'euid': self.uuid,
    }
    nfiles, fentries, subfolders = await putobjects(self, fentry, conn, now)
    files = files + nfiles
    t2d = lambda t: [dict(i) for i in t]

    await conn.execute(db.folder.insert(), fentry)

    sorter = lambda lst: sorted(t2d(lst),
        key=lambda i: (i.get("folder") or "") + "/" + i["name"]
    )
    fentries = sorter(subfolders) + sorter(fentries)
    flens = {}
    for foldent in fentries:
        flens[foldent["folder"]] = flens.get(foldent["folder"], 0) + 1
    flens = [{"uuid": self.uuid, "name": f, "len": n} for f, n in flens.items()]

    await conn.execute(db.folder_entry.insert(), fentries)
    await conn.execute(db.folder_length.insert(), flens)
    await conn.execute(db.account_entries.insert(), ants)
    flendat = ipc.request_ipc("update_other_user", uuid=self.userdata["uuid"], data={"foldlen": -1}) or {'foldlen': self.userdata['foldlen'] + 1}
    await conn.execute(db.account.update().where(db.account.c.uuid == self.userdata['uuid']), flendat)
    if len(files) > 0:
        await conn.execute(db.file.insert(), files)
    if tdata:
        await conn.execute(db.folder_thumb.insert(), tdata)
        await conn.execute(db.history.insert(), {
            "uuid": fentry["uuid"],
            "date": fentry["date"],
            "table": "folder",
            "field": "thumbnail",
            "data": fentry["thumb"],
        })
    await conn.execute(db.history.insert(), {
        "uuid": fentry["uuid"],
        "date": fentry["date"],
        "table": "folder",
        "field": "description",
        "data": fentry["description"],
    })
    await conn.commit()

    return fentry, tdata, files, self.userdata

async def commit_folder(self):
    async with db.db_engine.begin() as conn:
        if self.existing:
            # is an existing folder
            return await recommit_folder(self, conn)
        return await _commit_folder(self, conn)

###############################################################################

class UploadKicker(web.BaseHandler):
    def get(self, text=None):
        self.set_status(403)
        c = self.get_argument('next', '/')
        self.make_page('Uploads disabled', '<div class="fail-message">Uploads are disabled. Sorry.</div>', path=c)

class BaseUploadHandler(web.BaseHandler):
    def _send_to_status(self):
        if self._get_folder(False):
            self.redirect('/upload/status')
            return True
        return False
    def _finish_folder(self, foldat=None, existing=False):
        ipc.request_ipc("close", token=self._ftoken, user_token=self.token, foldat=foldat, existing=existing)
        self.clear_cookie("_upload_token")

class UploadHandler(BaseUploadHandler):
    @tornado.web.authenticated
    def get(self):
        if self._send_to_status():
            return
        self.make_page('Information about uploading', pages.get_fragment("pre-upload-page", text=pages.get_fragment('pre-upload-text', indent=16)))

    @tornado.web.authenticated
    def post(self):
        if self._send_to_status():
            return
        self.redirect('/upload/new')

class UploadFormHandler(BaseUploadHandler):
    @tornado.web.authenticated
    def get(self, msg='', name='', desc=''):
        if self._send_to_status():
            return

        catfrag = ''
        for cat in config.confdat.web.categories:
            catfrag += f'<option value="{cat}">{cat.capitalize()}</option>\n'

        self.make_page('Basic details', pages.get_fragment('upload-info-form', keys={
            'name': name,
            'categorylist': catfrag,
            'desc': desc,
            "maxsize": config.confdat.upload.thumb_max_size,
            "maxwidth": config.confdat.upload.thumb_max_width,
            "maxheight": config.confdat.upload.thumb_max_height,
        }, indent=0), msgtext=msg)

    @tornado.web.authenticated
    async def post(self):
        if self._send_to_status():
            return

        name = self.get_argument('name')
        desc = self.get_argument('desc', '')

        if not name or not name.strip():
            return self.get(msg='Name required', desc=desc)

        async with db.db_engine.begin() as conn:
            found = bool(await db.get_row(db.folder, 'name', name, conn, True))
        if found:
            return self.get(msg='Folder name already taken', desc=desc)

        category = self.get_argument('category', 'uncategorized')
        if thumb := self.request.files.get('thumbnail', None):
            thumb = thumb[0]['body']
        try:
            thumb = get_thumb_fent(thumb, name)
            folder = ipc.request_ipc("new_folder", user_token=self.token, userdata=self.current_user, name=name, desc=desc, thumb=thumb, category=category)
        except Exception as e:
            return self.get(msg=str(e), desc=desc)

        self.set_cookie("_upload_token", folder)
        self.redirect('/upload/status')

class AppendUploadHandler(BaseUploadHandler):
    @tornado.web.authenticated
    async def get(self, name):
        if self._send_to_status():
            return
        async with db.db_engine.begin() as conn:
            entry = await db.get_row(db.folder, 'name', name, conn, True)
            if not entry:
                return self.send404()
            if entry["creator"] != self.current_user["uuid"]:
                return self.make_page("Denied", '', msgtext=f"You're not the owner of the folder {name}")
            flist = await db.get_uuid(db.folder_entry, entry["uuid"], conn)

        flist = {(((i["folder"] + "/") if i["folder"] else "") + i["name"]): i for i in flist}

        if (inner := self.get_argument("inner")):
            inner = utils.norm(inner) or None
        try:
            folder = ipc.request_ipc("new_folder", user_token=self.token, userdata=self.current_user, name=name, exist_uuid=entry["uuid"], exist_list=flist, inner=inner)
        except Exception as e:
            return self.make_page("Refused", '', msgtext=f"Couldn't init upload session: {e!s}")

        self.set_cookie("_upload_token", folder)
        self.redirect('/upload/status')

class UploadFolderHandler(BaseUploadHandler):
    @tornado.web.authenticated
    def get(self):
        if not (folder := self._get_folder(floff=self.pagepair())):
            return

        fdrows = []
        for file in folder["flrows"]:
            extra = None
            if isinstance(file.pending, tuple):
                file.pending, extra = file.pending
            size = -1 if (file.failmsg or file.skipthis) else utils.get_human_size(file.size)
            if file.failmsg:
                block = [file._find, ("-[failed: " + file.failmsg + ']', extra), size, "[none]"]
            elif file.skipthis:
                block = [file._find, "-[skipped: " + file.skipthis + "]", size, "[none]"]
            elif file.pending:
                block = [file._find, ("-[status: " + file.pending + "]", extra), f'*{size}', "[unknown]"]
            else:
                block = [file._find, '+' + file.name, size, file.mime]
            fdrows.append(block)

        thumblink = ''
        if folder["thumb"]:
            thumblink = f'<img src="/upload/thumb.png?name={utils.url_escape(folder["name"])}" width="256" alt="thumbnail for current folder"/><br>'

        table = pages.build_table_frag('file_upload_entries', fdrows)

        inner = ""
        if folder["inner"]:
            inner = f'<br><div style="text-align: center">Currently appending to subfolder: <code>/{folder["inner"]}</code></div><br>'

        self.make_page(f'Uploading files to folder "{folder["name"]}"', pages.get_fragment('upload-file-form', keys={
            'name': folder["name"],
            "uptype": "appending to" if folder["existing"] else "creating",
            "size": utils.get_human_size(folder["size"]),
            'filecount': folder["flen"],
            'filetable': table,
            'page-links:pagelinks': datahands.gen_pagelinks(fdrows, self.page, self.page_size, folder["flen"]),
            "thumblink": thumblink,
            "description": folder["desc"],
            "maxsize": utils.get_human_size(config.confdat.data.max_upload_size),
            "maxsizebytes": config.confdat.data.max_upload_size,
            "currentlyat": inner,
        }, indent=0), msgtext=folder["msg"] or "")

class UploadThumbHandler(BaseUploadHandler):
    @tornado.web.authenticated
    def get(self, _):
        if not (folder := self._get_folder(False)):
            return
        if not folder["thumb"]:
            self.set_status(404)
            return

        self.set_header("Content-Type", folder["thumb"].mime)
        data = folder["thumb"].fread()
        self.set_header("Content-Length", str(len(data)))
        self.write(data)

@tornado.web.stream_request_body
class IncomingFile(BaseUploadHandler):
    _failed = False

    def _fail_msg(self, msg):
        ipc.request_ipc("pause_folder", token=self._ftoken, user_token=self.token, do_pause=False)
        ipc.request_ipc("set_fmsg", token=self._ftoken, user_token=self.token, msg=msg)
        if self.find is not None:
            ipc.request_ipc("pop_find", token=self._ftoken, user_token=self.token, find=self.find)
            self.find = None

    async def prepare(self):
        self.current_user = self.get_current_user()
        folder = self._get_folder()
        if not self.current_user or not folder:
            self.redirect('/upload')
            self._failed = True
            return

        self.limit = config.confdat.data.max_upload_size
        self.bytes_read = 0

        dlen = self.request.headers.get('Content-Length')
        if dlen is not None and self._check_limit(int(dlen)):
            self.send_error(413)
            self._failed = True
            return

        self._folder = folder
        if (ctype := self.request.headers.get("Content-Type")):
            ctype = utils.parse_header_line(ctype)
        if not ctype or ctype[1] != "multipart/form-data":
            self.send_error(400)
            self._failed = True
            return

        def threadform(stream, q):
            while (n := q.get()):
                stream.update(n)
            try:
                return stream.finish()
            except errors.BrokenFormError as e:
                return str(e)
        self._queue = queue.Queue(maxsize=16)
        self._stream = datahands.StreamedFormHandler(ctype[2])
        self._thread = await thread.spawn_thread("StreamForm", threadform, self._stream, self._queue)
        self._squeue = b''

        ipc.request_ipc("pause_folder", token=self._ftoken, user_token=self.token, do_pause=True)
        self.find = ipc.request_ipc("new_file", token=self._ftoken, user_token=self.token, init_msg="form data in progress")

    async def data_received(self, chunk):
        if self._failed:
            return

        self.bytes_read += len(chunk)

        if self._check_limit():
            self.bytes_read = 0
            self.send_error(413)
            self._fail_msg("Form denied due to upload limits.")
            self._failed = True
            return

        self._squeue += chunk
        ipc.request_ipc("size_file", token=self._ftoken, user_token=self.token, find=self.find, size=self.bytes_read)
        while len(self._squeue) > 4096:
            self._queue.put(self._squeue[:4096])
            self._squeue = self._squeue[4096:]

    def _backtostats(self, dofile=True):
        if self.find is not None:
            ipc.request_ipc("skip_file", token=self._ftoken, user_token=self.token, find=self.find, msg="returned to status page")
        if dofile:
            self._stream.close_files()
        self.redirect("/upload/status")

    @tornado.web.authenticated
    async def post(self):
        if self._failed:
            return

        folder = self._folder
        if self._squeue:
            self._queue.put(self._squeue)
        self._queue.put(None)
        ret = thread.join(self._thread)
        if isinstance(ret, str):
            self._fail_msg(f"Invalid form data. This might be (a small chance) your browser's fault. ({ret})")
            return self._backtostats()
        stream = self._stream

        if "cancel" in stream.keys:
            self._finish_folder()
            stream.close_files()
            if folder["existing"]:
                if (inner := folder["inner"] or ""):
                    inner = "/" + inner
                self.redirect(f"/folder/{utils.url_escape(folder['name'])}{inner}")
            else:
                self.redirect("/")
            return

        if 'upload_url' in stream.keys:
            rurl = None
            if not isinstance(stream.keys["url"], list):
                rurl = url.check_url(stream.keys['url'].decode())
            if not rurl:
                self._fail_msg("Please submit a valid URL.")
                return self._backtostats()

            rurl = rurl.geturl()
            find = ipc.request_ipc("new_file", token=self._ftoken, user_token=self.token, init_msg="downloading: " + rurl)
            ipc.request_ipc("pop_find", token=self._ftoken, user_token=self.token, find=self.find)
            self.find = None
            datahands.FileEntry.from_url(rurl, [self._ftoken, self.token, find])
            return self._backtostats()

        if not stream.files and 'confirm' not in stream.keys:
            self._fail_msg("Please upload a file. (If you did, then its name was wrong; try again with a different name.)")
            return self._backtostats()

        isarc = "upload_arc" in stream.keys and not ("upload" in stream.keys or "confirm" in stream.keys)
        finds = await thread.run_in_thread(datahands.submitfiles, self._ftoken, self.token, stream.files, isarc=isarc)
        if isinstance(finds, str):
            self._fail_msg(finds)
            return self._backtostats()

        ipc.request_ipc("pop_find", token=self._ftoken, user_token=self.token, find=self.find)
        self.find = None

        if 'confirm' not in stream.keys:
            return self._backtostats(False)

        comdata = ipc.request_ipc("commit", token=self._ftoken, user_token=self.token)
        if isinstance(comdata, str):
            self._fail_msg(comdata)
            return self._backtostats(False)

        fentry = await commit_folder(comdata)
        self._finish_folder(fentry, existing=bool(comdata.existing))
        stream.close_files()

        if (inner := folder["inner"] or ""):
            inner = "/" + inner
        self.redirect(f'/folder/{folder["name"]}{inner}')
