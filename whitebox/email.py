from . import utils # noqa: F401
import email.utils

def check_email(addr):
    addr = email.utils.parseaddr(addr)
    if not addr[1]:
        return False
    addr = addr[1]
    username, _, host = addr.partition("@")
    if not _ or "@" in host or "." not in host or ":" in username:
        return False
    return True
