from . import db, pages, web, datahands, utils, ipc
import time, tornado.web

def color_user(udat, withlink=False):
    username = udat["username"]
    tag = ("a", f' href="/user/{utils.url_escape(username)}"') if withlink else ("span", "")
    return '<{}{} style="color: {}">{}</{}>'.format(tag[0], tag[1], UserType[udat["type"]][1], utils.xhtml_escape(username), tag[0])

UserSType = {
    "unknown": -1,
    "normal": 0,
    "moderator": 1,
    "admin": 2,
    "banned": 3,
}
UserType = {
    UserSType["unknown"]: ("unknown", "yellow"),
    UserSType["normal"]: ("normal", "#00cef8"),
    UserSType["moderator"]: ("moderator", "lightgreen"),
    UserSType["admin"]: ("admin", "red"),
    UserSType["banned"]: ("banned", "gray"),
}

class UserHandler(web.BaseHandler):
    async def _admctl(self, conn, user):
        pushdat = {}
        if promote := self.get_argument("promote", allowed=("normal", "moderator", "admin")):
            wasbanned = user["type"] == UserSType["banned"]
            pushdat["type"] = UserSType[promote]
            if wasbanned:
                pushdat["banuuid"] = pushdat["banreason"] = None
        if api_allow := self.get_argument("api_allow", allowed=("yes", "no")):
            pushdat["api_allowed"] = api_allow == "yes"
        if doactivate := self.get_argument("doactivate", allowed=('0', '1')):
            pushdat["activated"] = doactivate == '1'

        if pushdat:
            user |= pushdat
            ipc.request_ipc("update_other_user", uuid=user["uuid"], data=pushdat)
            await conn.execute(db.account.update().where(
                db.account.c.uuid == user['uuid']
            ), pushdat)

    async def get(self, username, msgback=''):
        user = await db.get_user(username)
        if not user:
            return self.send404()

        flrows = []
        tfrag = ''
        async with db.db_engine.begin() as conn:
            if self.isadmin() and user["username"] != self.current_user["username"]:
                await self._admctl(conn, user)

            async for i in await db.get_rows(db.folder, "creator", user['uuid'], conn, offpair=self.pagepair(), orderby='date'):
                i = await db.reformat_folder(i, conn)
                flrows.append([i["thumb"], i['name'], i['date'], i['category'], user, (i["views"], i["favourites"])])

        tfrag = pages.build_table_frag('folder_results', flrows, klasse="list")

        elinks = []
        if user["public_favourites"]:
            elinks.append(f'<a href="/user/{username}/favourites">{username}\'s favourites</a>')

        if self.current_user and self.current_user["username"] == username:
            elinks.append('<a href="/settings">Settings page</a>')

        if self.isadmin() and user["username"] != self.current_user["username"]:
            admincontrols = "<div>Make user a(n): |"
            lnk = lambda ntype, dest='': f' <a href="{dest or "?promote=" + ntype}" style="color: {UserType[UserSType[ntype]][1]}">{ntype}</a> |'
            for ut in filter(lambda i: user["type"] != UserSType[i[0]],
                    (("normal", 0), ("moderator", 0),
                     ("admin", 0), ("banned", f"/user/{username}/ban"))
                ):
                admincontrols += lnk(ut[0], ut[1])

            if user["type"] != UserSType["admin"]:
                aarg, atxt = ("no", "Revoke") if user["api_allowed"] else ("yes", "Allow")
                admincontrols += f'<br><a href="/user/{username}?api_allow={aarg}">{atxt} user\'s API access</a>'
            actext = ("Dea" if user["activated"] else "A") + "ctivate this account"
            admincontrols += f'<br><a href="?doactivate={int(not user["activated"])}">{actext}</a></div>'
            elinks.append(admincontrols)


        banreason = ""
        if user["type"] == UserSType["banned"]:
            banadmin = await db.get_uuid(db.account, user["banuuid"], None, True)
            banreason = " by " + color_user(banadmin) + "<br>Ban reason: " + utils.xhtml_escape(user["banreason"])

        if elinks:
            elinks.insert(0, "")
        desc = user["description"] or ""
        self.make_page('User profile', pages.get_fragment('user-pages', keys={
            'name': color_user(user),
            "email": f' (<a href="mailto:{user["email"]}">{user["email"]}</a>)' if self.isadmin() and user["email"] else "",
            'uuid': user['uuid'],
            "creationtime": time.ctime(user["date"]),
            "foldercount": user["foldlen"],
            'foldertable': tfrag,
            'page-links:pagelinks': datahands.gen_pagelinks(flrows, self.page, self.page_size, user["foldlen"]),
            'usertype': UserType[user["type"]][0] + banreason,
            "description": desc,
            "elinks": "<br>".join(elinks),
        }), msgtext=msgback)

class BanHandler(UserHandler):
    async def _getuser(self, username):
        user = await db.get_user(username)
        if not user:
            return self.send404()

        btxt = None
        if self.current_user["type"] != UserSType["admin"]:
            btxt = "Only admins can ban a user."
        elif username == self.current_user["username"]:
            btxt = "You tried to ban yourself (extra silly)."
        elif user["type"] == UserSType["banned"]:
            btxt = "This user is already banned."
        if btxt:
            return await super().get(username, btxt)
        return user

    @tornado.web.authenticated
    async def get(self, username, text=None):
        if not await self._getuser(username):
            return
        token = self.get_csrf("/user/" + username + "/ban")
        self.make_page(f'Ban form for {username}',
            pages.get_fragment('ban-form', keys={
                "token": token,
                "target": username,
            }, indent=0), msgtext=(text or None)
        )
    @tornado.web.authenticated
    async def post(self, username):
        user = await self._getuser(username)
        if not self.verify_csrf():
            return await self.get('CSRF token validation failed.')
        async with db.db_engine.begin() as conn:
            data = {
                "type": UserSType["banned"],
                "banuuid": self.current_user["uuid"],
                "banreason": self.get_argument("reason")
            }
            ipc.request_ipc("update_other_user", uuid=user["uuid"], data=data)
            await conn.execute(db.account.update().where(
                db.account.c.uuid == user['uuid']
            ), data)
        return await super().get(username, "User banned")

class FavouriteHandler(web.BaseHandler):
    async def get(self, username):
        user = await db.get_user(username)
        if not user:
            return self.send404()
        flrows = []

        if not user["public_favourites"] and (not self.current_user or self.current_user["username"] != username):
            return self.send404("User has hidden their favourites list.")

        async with db.db_engine.begin() as conn:
            async for entry in await db.get_rows(db.favourites, "uuid", user["uuid"], conn, offpair=self.pagepair()):
                i = await db.reformat_folder(await db.get_uuid(db.folder, entry['euid'], conn, True), conn)
                flrows.append([i["thumb"], i['name'], i['date'], i['category'], i['creator'], (i["views"], i["favourites"])])

        tfrag = pages.build_table_frag('folder_results', flrows, klasse="list")

        self.make_page(f"{username}'s favourites", pages.get_fragment('favourites-page', keys={
            'name': color_user(user, withlink=True),
            "foldercount": user['favslen'],
            'foldertable': tfrag,
            'page-links:pagelinks': datahands.gen_pagelinks(flrows, self.page, self.page_size, user['favslen']),
        }))
