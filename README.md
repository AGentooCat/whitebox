# WhiteBox
WhiteBox is a self-hostable Internet Archive clone focused on simplicity licensed under [MIT License](./LICENSE).
Its features are:

- Form-based and API-based folder creations supporting single files, archive imports (zip and tar) and URL download support
- Special handling of URLs via plugin support
- Exporting folders (zip, tar and tar.gz)
- Thumbnail support for folders (JPEG and PNG, 1024x1024 and 1MB max)
- Easy to navigate and page-based folder viewing
- All files saved alongside their Blake2b hashes
- Simple account system (uploading folders, favourite lists)

## Installation

### Auto-install

You can install this package from this repo with pip/uv's git+https schema support:
```
$ virtualenv wbox
$ source wbox/bin/activate(.fish/.csh/...)
$ pip install git+https://codeberg.org/AGentooCat/whitebox.git
```

### Manual-install

WhiteBox depends on these packages, if you're installing manually:

- tornado & pycurl
- sqlalchemy
- pillow
- file-magic
- zipstream-ng
- werkzeug
- html5lib & cssutils

You also need an async-driver for your DBMS that WhiteBox (with SQLAlchemy) will use.

- SQLite: `aiosqlite`
- PostgreSQL: `asyncpg`
- MySQL/MariaDB: `pymysql` + `aiomysql`

These will be installed automatically if WhiteBox is installed with the related feature: `sqlite / postgres / mysql`.

## Configuration
The file `whitebox/config.py` lists available configs with their defaults, and `example.toml` gives an example to go off of.
Most likely, you will replace `main.datadir`, `web.address/port` and `database` sections.

For running as non-root, make sure that:

- The `--logfile=/var/log/whitebox.log` and `--pidfile=/run/whitebox.pid` can be created by the current user.
- The post number is above 1023 (or that you can bind to special ports)
- The `main.datadir` config can be created and writeable.

## Running
If installed with a package manager, simply running `whitebox [-c config]` will run WhiteBox as a daemon, printing its PID. You can also run WhiteBox from the repo folder with `python -m whitebox`.

When running for the first time, WhiteBox will generate an admin account with a randomized password. You can change both the name and the password on the account settings page.
