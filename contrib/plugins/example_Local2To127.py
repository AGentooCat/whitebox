import whitebox.plugin as pl
from urllib.parse import urlparse

class LocalTo127(pl.BasePlugin):
    _category = "PreURLHandler"

    def _gethost(self, url):
        return urlparse(url).netloc.partition(":")[0]
    def _changehost(self, url, host):
        data = urlparse(url)
        port = data.netloc.partition(":")[2]
        if port:
            port = ":" + port
        return data._replace(netloc=host + port).geturl()
    def can_run(self, data):
        return self._gethost(data) == "localhost2"
    def run(self, data):
        if not self.can_run(data):
            return None
        return self._changehost(data, "127.0.0.1")

plugins = [LocalTo127]
