import whitebox.plugin, json

class JSONExport(whitebox.plugin.BasePlugin):
    _category = "ExportFormat"
    _lazyinit = True

    files = {}
    def can_run(self, fmt):
        return fmt == "json"

    def run(self, fmt):
        return (self, "application/json", True)

    def add(self, name, fent):
        if fent:
            if not isinstance(fent["hash"], str):
                fent["hash"] = fent["hash"].hex()
        self.files[name] = fent

    def read(self, size=8192):
        if not self.files:
            return b''
        d = json.dumps(self.files)
        self.files.clear()
        return d.encode()

    def close(self):
        self.files.clear()
plugins = [JSONExport]
